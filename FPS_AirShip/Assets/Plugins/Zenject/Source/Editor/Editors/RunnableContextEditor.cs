﻿#if !ODIN_INSPECTOR

using UnityEditor;

namespace Zenject
{
    [NoReflectionBaking]
    public class RunnableContextEditor : ContextEditor
    {
        SerializedProperty _autoRun;
        SerializedProperty _canAutoRunInBuildProperty;

        public override void OnEnable()
        {
            base.OnEnable();

            _autoRun = serializedObject.FindProperty("_autoRun");
            _canAutoRunInBuildProperty = serializedObject.FindProperty("_canAutoRunInBuild");
        }

        protected override void OnGui()
        {
            base.OnGui();

            EditorGUILayout.PropertyField(_autoRun);
            EditorGUILayout.PropertyField(_canAutoRunInBuildProperty);
        }
    }
}


#endif
