﻿using System;
using UnityEngine;
using Zenject;

namespace PdUtils.SceneLoadingProcessor.Impls
{
    public class MenuTweenProcess : Process
    {
        private AsyncOperation _operation;
        private Action _complete;

        private SignalBus _signalBus;

        public MenuTweenProcess(SignalBus signalBus)
        {
            _signalBus = signalBus;
        }

        public override void Do(Action onComplete)
        {
            _complete = onComplete;
            _operation.completed += OnCompleted;
          //  _signalBus.Fire(new SignalBlurScreen(false));
        }

        private void OnCompleted(AsyncOperation obj)
        {
            _operation.completed -= OnCompleted;
            _complete();
        }
    }
}