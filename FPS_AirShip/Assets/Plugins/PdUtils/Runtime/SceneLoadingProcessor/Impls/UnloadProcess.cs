using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PdUtils.SceneLoadingProcessor.Impls
{
	public class UnloadProcess : Process
	{
		private readonly string _sceneName;
		private AsyncOperation _operation;
		private Action _complete;
		private Func<bool> _condition;

		public UnloadProcess(string sceneName, Func<bool> condition = null)
		{
			_sceneName = sceneName;
			_condition = condition;
		}

		public override void Do(Action complete)
		{
			if (_condition != null && !_condition.Invoke())
			{
				complete();
				return;
			}

			_complete = complete;
			_operation = SceneManager.UnloadSceneAsync(_sceneName);
			_operation.completed += OnUnloadSceneCompleted;
		}

		private void OnUnloadSceneCompleted(AsyncOperation obj)
		{
			_operation.completed -= OnUnloadSceneCompleted;
			_operation = Resources.UnloadUnusedAssets();
			_operation.completed += OnUnloadUnusedAssetsCompleted;
		}

		private void OnUnloadUnusedAssetsCompleted(AsyncOperation obj)
		{
			_operation.completed -= OnUnloadUnusedAssetsCompleted;
			_complete();
		}
	}
}