﻿using System;
using UniRx;
using CustomSelectables;

namespace Utils.UiExtensions
{
    public static class CustomUIComponentExtensions
    {
        /// <summary>Observe onClick event.</summary>
        public static IObservable<Unit> OnClickAsObservable(this CustomButton button)
        {
            return button.onClick.AsObservable();
        }
        
        /// <summary>Observe onValueChanged with current `isOn` value on subscribe.</summary>
        public static IObservable<bool> OnValueChangedAsObservable(this CustomToggle toggle)
        {
            // Optimized Defer + StartWith
            return Observable.CreateWithState<bool, CustomToggle>(toggle, (t, observer) =>
            {
                observer.OnNext(t.isOn);
                return t.onValueChanged.AsObservable().Subscribe(observer);
            });
        }
        
        /// <summary>Observe onValueChanged with current `value` value on subscribe.</summary>
        public static IObservable<float> OnValueChangedAsObservable(this CustomSlider slider)
        {
            // Optimized Defer + StartWith
            return Observable.CreateWithState<float, CustomSlider>(slider, (t, observer) =>
            {
                observer.OnNext(t.value);
                return t.onValueChanged.AsObservable().Subscribe(observer);
            });
        }
        
        /// <summary>Observe onValueChanged with current `value` value on subscribe.</summary>
        public static IObservable<int> OnValueChangedAsObservable(this Switcher switcher)
        {
            // Optimized Defer + StartWith
            return Observable.CreateWithState<int, Switcher>(switcher, (t, observer) =>
            {
                observer.OnNext(t.value);
                return t.onValueChanged.AsObservable().Subscribe(observer);
            });
        }
    }
}