﻿using UnityEngine.EventSystems;

namespace Plugins.SimpleUi.CustomSelectables
{
    public class CustomEventSystem : EventSystem
    {
        protected override void Awake()
        {
            base.Awake();
            base.Update();
        }
    }
}