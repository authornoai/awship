﻿using System;
using System.Collections.Generic;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace CustomSelectables
{
    public class Switcher : CustomSelectable
    {
        [SerializeField] private TMP_Text textState;
        [SerializeField] private Button prevBtn;
        [SerializeField] private Button nextBtn;
        
        [Serializable]
        public class SwitcherEvent : UnityEvent<int> {}
        
        [SerializeField]
        private SwitcherEvent m_OnValueChanged = new SwitcherEvent();
        public SwitcherEvent onValueChanged 
        { 
            get => m_OnValueChanged;
            set => m_OnValueChanged = value;
        }

        [SerializeField]
        private List<string> m_Options = new List<string>();
        
        public List<string> options
        {
            get => m_Options;
            set { m_Options = value; RefreshShownValue(); }
        }

        [SerializeField]
        private int m_Value;
        
        public int value
        {
            get => m_Value;
            set => SetValue(value);
        }
        
        public void SetValueWithoutNotify(int newValue)
        {
            SetValue(newValue, false);
        }

        private void SetValue(int newValue, bool sendCallback = true)
        {
            if (Application.isPlaying && 
                (options == null || options.Count == 0 ||
                 newValue == m_Value || 
                 newValue < 0 || 
                 newValue == options.Count))
                return;

            m_Value = Mathf.Clamp(newValue, 0, m_Options.Count - 1);
            
            RefreshShownValue();

            if (!sendCallback) return;
            
            m_OnValueChanged.Invoke(m_Value);
        }
        
        protected override void Awake()
        {
            base.Awake();
            prevBtn.OnClickAsObservable().Subscribe(x => value--).AddTo(prevBtn);
            nextBtn.OnClickAsObservable().Subscribe(x => value++).AddTo(prevBtn);
        }

        protected override void Start()
        {
            RefreshShownValue();
        }
        
#if UNITY_EDITOR
        protected override void OnValidate()
        {
            base.OnValidate();

            if (!IsActive()) return;

            m_Value = m_Options == null || m_Options.Count == 0 ? 0 : Mathf.Clamp(m_Value, 0, m_Options.Count - 1);

            RefreshShownValue();
        }
#endif
        
        public void RefreshShownValue()
        {
            var text = "No options";

            if (m_Options != null && m_Options.Count != 0)
            {
                text = m_Options[Mathf.Clamp(m_Value, 0, m_Options.Count - 1)];
                prevBtn.interactable = m_Value != 0;
                nextBtn.interactable = m_Value != m_Options.Count - 1;
            }
            else
            {
                prevBtn.interactable = false;
                nextBtn.interactable = false;
            }

            textState.text = text;
            
        }

        public override void OnMove(AxisEventData eventData)
        {
            switch (eventData.moveDir)
            {
                case MoveDirection.Left:
                    value--;
                    break;
                case MoveDirection.Right:
                    value++;
                    break;
                case MoveDirection.Up:
                    base.OnMove(eventData);
                    break;
                case MoveDirection.Down:
                    base.OnMove(eventData);
                    break;
            }
        }
    }
}