﻿using UnityEditor;

namespace CustomSelectables
{
    [CustomEditor(typeof(Switcher), true)]
    public class SwitcherEditor : CustomSelectablesEditor
    {
        private SerializedProperty _textState;
        private SerializedProperty _prevBtn;
        private SerializedProperty _nextBtn;
        private SerializedProperty _options;
        private SerializedProperty _value;
        private SerializedProperty _onValueChanged;
        
        protected override void OnEnable()
        {
            base.OnEnable();
            _textState = serializedObject.FindProperty("textState");
            _prevBtn = serializedObject.FindProperty("prevBtn");
            _nextBtn = serializedObject.FindProperty("nextBtn");
            _options = serializedObject.FindProperty("m_Options");
            _value = serializedObject.FindProperty("m_Value");
            _onValueChanged = serializedObject.FindProperty("m_OnValueChanged");
        }
        
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            EditorGUILayout.Space();

            serializedObject.Update();

            EditorGUILayout.PropertyField(_textState);
            EditorGUILayout.PropertyField(_prevBtn);
            EditorGUILayout.PropertyField(_nextBtn);
            EditorGUILayout.PropertyField(_options);
            EditorGUILayout.PropertyField(_value);

            serializedObject.ApplyModifiedProperties();
        }
        
        protected override void DrawEvents()
        {
            base.DrawEvents();
            EditorGUILayout.PropertyField(_onValueChanged);
        }
    }
}