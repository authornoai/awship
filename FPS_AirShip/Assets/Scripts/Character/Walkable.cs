﻿using UnityEngine;
using Zenject;

namespace AWShip.Character
{
    [RequireComponent(typeof(CharacterController))]
    public sealed class Walkable : MonoBehaviour, IInitializable, ITickable
    {
        [Header("Move")]
        [SerializeField] private float _walkingSpeed;
        [SerializeField] private float _runningSpeed;
        
        [Header("Gravity")]
        [SerializeField] private float _jumpPower;
        [SerializeField] private float _gravity;

        [Header("Internal")] 
        private Transform _transform;
        private CharacterController _charController;
        
        private Vector3 _movementDir;
        private Vector3 _movementForce;

        private bool _canMove = true;
        private bool _isNotRunning = true;
        private bool _isGrounded = false;

        private Vector3 _scheldueMove;

        public void Initialize()
        {
            _charController = GetComponent<CharacterController>();
            _transform = transform;

            //TODO: Transfer to CursorService
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

        public void SetMoveDir(Vector3 target) => _movementDir = target;
        public void SetRunning(bool state) => _isNotRunning = !state;
        public void DoJump()
        {
            if (_canMove && _isGrounded) _movementForce.y = _jumpPower;
        }


        public void Tick() => DoMove();
        private void DoMove()
        {
            _isGrounded = _charController.isGrounded;
            float movementForceY = _movementForce.y;
            Vector3 localMovement = _transform.TransformDirection(Vector3.forward) * _movementDir.z
                                    + _transform.TransformDirection(Vector3.right) * _movementDir.x;
            
            _movementForce = _canMove
                ? _isNotRunning ? localMovement * _walkingSpeed : localMovement * _runningSpeed
                : Vector3.zero;
            
            _movementForce.y = movementForceY;
            if (!_isGrounded) _movementForce.y -= _gravity * Time.fixedDeltaTime;
            
            _charController.Move(_movementForce * Time.deltaTime + _scheldueMove);
            _scheldueMove = Vector3.zero;
        }

        public void ScheduleMoveOnTick(Vector3 deltaMove) => _scheldueMove = deltaMove;
    }
}