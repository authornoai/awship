﻿using UnityEngine;
using Zenject;

namespace AWShip.Character
{
    public class Lookable : MonoBehaviour, ITickable
    {
        [Header("Settings")] 
        [SerializeField] private float _lookSpeed;
        [SerializeField] private float _lookXLimit;
        
        [Header("Targets")]
        [SerializeField] private Transform _lookTargetUp;
        [SerializeField] private Transform _lookTargetRight;
        
        private float _rotationUp;
        private Vector2 _rotationDir;
        private bool _canLook = true;

        public void Tick() => DoLook();
        private void DoLook()
        {
            if (!_canLook) return;
            
            _rotationUp += _lookSpeed * -_rotationDir.y * Time.deltaTime;
            _rotationUp = Mathf.Clamp(_rotationUp, -_lookXLimit, _lookXLimit);
            _lookTargetUp.localRotation = Quaternion.Euler(_rotationUp, 0, 0);
            
            _lookTargetRight.localRotation *= Quaternion.Euler(0, _rotationDir.x * _lookSpeed * Time.deltaTime, 0);
        }

        public void SetRotationDir(Vector2 dir) => _rotationDir = dir;
    }
}