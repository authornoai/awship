﻿namespace AWShip.Core.StateMachine
{
    public interface IFsmState
    {
        void OnEnter();
        void OnUpdate();
        void OnExit();
    }
}