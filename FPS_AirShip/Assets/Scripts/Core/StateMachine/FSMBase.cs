﻿using System;
using System.Collections.Generic;

namespace AWShip.Core.StateMachine
{
    public class FsmBase
    {
        private Dictionary<IFsmState, List<FsmTransition>> _transitions;
        private List<IFsmState> _states;
        
        private IFsmState _stateLast;
        private IFsmState _stateCurrent;

        public FsmBase(IFsmState state, int usedStates)
        {
            _stateLast = state;
            _stateCurrent = state;
            _states = new List<IFsmState>();
            _transitions = new Dictionary<IFsmState, List<FsmTransition>>(usedStates);
        }

        public void AddTransition(IFsmState from, IFsmState to, Func<bool> check)
        {
            if (!_transitions.ContainsKey(from))
            {
                _transitions.Add(from, new List<FsmTransition>());
                _states.Add(from);
            }
            
            _transitions[from].Add(new FsmTransition(to, check));
        }

        public void AddTransition(IFsmState a, IFsmState b, Func<bool> fromAtoB, Func<bool> fromBtoA)
        {
            AddTransition(a, b, fromAtoB);
            AddTransition(b, a, fromBtoA);
        }

        public void Update()
        {
            if(!_transitions.ContainsKey(_stateCurrent)) return;

            List<FsmTransition> transitions = _transitions[_stateCurrent];
            foreach (FsmTransition transition in transitions)
            {
                if (!transition.Check.Invoke()) continue;
                ChangeRunningState(transition.To);
                return;
            }
            
            _stateCurrent.OnUpdate();
        }

        private void ChangeRunningState(IFsmState to)
        {
            _stateCurrent.OnExit();
            to.OnEnter();
            
            _stateLast = _stateCurrent;
            _stateCurrent = to;
        }

        private sealed class FsmTransition
        {
            public FsmTransition(IFsmState to, Func<bool> check)
            {
                _to = to;
                _check = check;
            }

            private IFsmState _to;
            private Func<bool> _check;

            public IFsmState To => _to;
            public Func<bool> Check => _check;
        }
    }
}