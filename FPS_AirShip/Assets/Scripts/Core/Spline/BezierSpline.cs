﻿using UnityEngine;
using System;
using Core.Spline;

public class BezierSpline : MonoBehaviour {

	[SerializeField] private Vector3[] _points;
	[SerializeField] private BezierControlPointMode[] _pointModes;
	[SerializeField] private bool _isLoop;
	
	[SerializeField] private float _maxLenght;
	[SerializeField] private FloatLimit2D[] _pointLimits;
	
	public int CurveCount => (_points.Length - 1) / 3;
	
	public Vector3 GetPointLenght(float lenght)
	{
		float t = lenght / _maxLenght;
		int i = _points.Length - 4;;
		float innerT = 1f;

		if (t < 1f) {
			t = Mathf.Clamp01(t);
			for (int c = CurveCount - 1; c >= 0; --c)
			{
				if (lenght > _pointLimits[c].Min)
				{
					innerT = (lenght - _pointLimits[c].Min) / (_pointLimits[c].Max - _pointLimits[c].Min);
					i = c * 3;
					break;
				}
			}
		}

		return transform.TransformPoint(BezierUtility.GetPoint(_points[i], _points[i + 1], _points[i + 2], _points[i + 3], innerT));
	}
	
	public Vector3 GetPoint (float t) {
		int i;
		if (t >= 1f) {
			t = 1f;
			i = _points.Length - 4;
		}
		else {
			t = Mathf.Clamp01(t) * CurveCount;
			i = (int)t;
			t -= i;
			i *= 3;
		}
		return transform.TransformPoint(BezierUtility.GetPoint(_points[i], _points[i + 1], _points[i + 2], _points[i + 3], t));
	}
	public Vector3 GetVelocity (float t) {
		int i;
		if (t >= 1f) {
			t = 1f;
			i = _points.Length - 4;
		}
		else {
			t = Mathf.Clamp01(t) * CurveCount;
			i = (int)t;
			t -= i;
			i *= 3;
		}
		return transform.TransformPoint(BezierUtility.GetFirstDerivative(_points[i], _points[i + 1], _points[i + 2], _points[i + 3], t)) - transform.position;
	}
	public Vector3 GetDirection (float t) {
		return GetVelocity(t).normalized;
	}

#if UNITY_EDITOR
	
	public void Reset () {
		_points = new Vector3[] {
			new Vector3(1f, 0f, 0f),
			new Vector3(2f, 0f, 0f),
			new Vector3(3f, 0f, 0f),
			new Vector3(4f, 0f, 0f)
		};
		_pointModes = new BezierControlPointMode[] {
			BezierControlPointMode.Free,
			BezierControlPointMode.Free
		};
		
		UpdateLenght();
	}
	
	public bool Loop {
		get => _isLoop;
		set {
			_isLoop = value;
			if (value != true) return;
			_pointModes[_pointModes.Length - 1] = _pointModes[0];
			SetControlPoint(0, _points[0]);
		}
	}
	public Vector3 GetControlPoint (int index) => _points[index];
	public BezierControlPointMode GetControlPointMode(int index) => _pointModes[(index + 1) / 3];
	public int ControlPointCount => _points.Length;
	
	public void SetControlPoint (int index, Vector3 point) {
		if (index % 3 == 0) {
			Vector3 delta = point - _points[index];
			if (_isLoop) {
				if (index == 0) {
					_points[1] += delta;
					_points[_points.Length - 2] += delta;
					_points[_points.Length - 1] = point;
				}
				else if (index == _points.Length - 1) {
					_points[0] = point;
					_points[1] += delta;
					_points[index - 1] += delta;
				}
				else {
					_points[index - 1] += delta;
					_points[index + 1] += delta;
				}
			}
			else {
				if (index > 0) {
					_points[index - 1] += delta;
				}
				if (index + 1 < _points.Length) {
					_points[index + 1] += delta;
				}
			}
		}
		_points[index] = point;
		EnforceMode(index);
		UpdateLenght();
	}
	public void SetControlPointMode (int index, BezierControlPointMode mode) {
		int modeIndex = (index + 1) / 3;
		_pointModes[modeIndex] = mode;
		if (_isLoop) {
			if (modeIndex == 0) {
				_pointModes[_pointModes.Length - 1] = mode;
			}
			else if (modeIndex == _pointModes.Length - 1) {
				_pointModes[0] = mode;
			}
		}
		EnforceMode(index);
		UpdateLenght();
	}
	
	public void AddCurve () {
		Vector3 point = _points[_points.Length - 1];
		Array.Resize(ref _points, _points.Length + 3);
		point.x += 1f;
		_points[_points.Length - 3] = point;
		point.x += 1f;
		_points[_points.Length - 2] = point;
		point.x += 1f;
		_points[_points.Length - 1] = point;

		Array.Resize(ref _pointModes, _pointModes.Length + 1);
		_pointModes[_pointModes.Length - 1] = _pointModes[_pointModes.Length - 2];
		EnforceMode(_points.Length - 4);
		UpdateLenght();

		if (!_isLoop) return;
		
		_points[_points.Length - 1] = _points[0];
		_pointModes[_pointModes.Length - 1] = _pointModes[0];
		EnforceMode(0);
		UpdateLenght();
	}
	private void EnforceMode (int index) {
		int modeIndex = (index + 1) / 3;
		BezierControlPointMode mode = _pointModes[modeIndex];
		if (mode == BezierControlPointMode.Free 
		    || !_isLoop && (modeIndex == 0 || modeIndex == _pointModes.Length - 1)) {
			return;
		}

		int middleIndex = modeIndex * 3;
		int fixedIndex, enforcedIndex;
		if (index <= middleIndex) {
			fixedIndex = middleIndex - 1;
			if (fixedIndex < 0) {
				fixedIndex = _points.Length - 2;
			}
			enforcedIndex = middleIndex + 1;
			if (enforcedIndex >= _points.Length) {
				enforcedIndex = 1;
			}
		}
		else {
			fixedIndex = middleIndex + 1;
			if (fixedIndex >= _points.Length) {
				fixedIndex = 1;
			}
			enforcedIndex = middleIndex - 1;
			if (enforcedIndex < 0) {
				enforcedIndex = _points.Length - 2;
			}
		}

		Vector3 middle = _points[middleIndex];
		Vector3 enforcedTangent = middle - _points[fixedIndex];
		if (mode == BezierControlPointMode.Aligned) {
			enforcedTangent = enforcedTangent.normalized * Vector3.Distance(middle, _points[enforcedIndex]);
		}
		_points[enforcedIndex] = middle + enforcedTangent;
	}

	private void UpdateLenght()
	{
		const int numOfIteration = 1024;
		int curveIndex = 0;
		_maxLenght = -1;
		
		Vector3 lastPoint = BezierUtility.GetFirstDerivative(_points[curveIndex], _points[curveIndex + 1], _points[curveIndex + 2], _points[curveIndex + 3], 0.0f);

		_pointLimits = new FloatLimit2D[CurveCount];
		for (int c = 0; c < CurveCount; ++c)
		{
			_pointLimits[c].Min = _maxLenght;
			curveIndex = c * 3;
			
			for (int i = 0; i < numOfIteration; ++i)
			{
				float currentTime = (i / (float)numOfIteration);
				Vector3 newPoint = BezierUtility.GetFirstDerivative(_points[curveIndex], _points[curveIndex + 1], _points[curveIndex + 2], _points[curveIndex + 3], currentTime);
				_maxLenght += (lastPoint - newPoint).magnitude;

				if (i == numOfIteration - 1)
				{
					_pointLimits[c].Max = _maxLenght;
				}
			}
			
		}
	}

#endif
}