﻿using System;
using UnityEngine;

namespace Core.Spline
{
    [Serializable]
    public struct FloatLimit2D
    {
        [SerializeField] private float _min;
        [SerializeField] private float _max;
        
        public float Min
        {
            get => _min;
            set => _min = value;
        }
        
        public float Max
        {
            get => _max;
            set => _max = value;
        }
    }
}