using AWShip.Character;
using AWShip.Input;
using GameSystems.Path;
using GameSystems.Ship.ShipAntigravity;
using GameSystems.Ship.ShipCore;
using GameSystems.Ship.ShipEngine;
using UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace AWShip.Init
{
    public class GameInstaller : MonoInstaller<GameInstaller>
    {
        [SerializeField] private Camera _camera;
        
        [SerializeField] private Walkable _playerWalk;
        [SerializeField] private Lookable _playerLook;

        //TODO: Move to another installer
        [Header("View")] 
        [SerializeField] private ShipView _viewShip;
        
        [SerializeField] private ShipAntigravityView _viewShipAntigravity;
        
        [SerializeField] private PlayerInteractorView _viewPlayerInter;
        
        
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<Camera>().FromInstance(_camera).AsSingle();
            
            Container.BindInterfacesAndSelfTo<Walkable>().FromInstance(_playerWalk).AsSingle();
            Container.BindInterfacesAndSelfTo<Lookable>().FromInstance(_playerLook).AsSingle();
            Container.BindInterfacesAndSelfTo<MasterInput>().AsSingle();
            Container.BindInterfacesAndSelfTo<PlayerInputLinker>().AsSingle();
            Container.BindInterfacesAndSelfTo<PlayerInteractor>().AsSingle();

            Container.BindInterfacesAndSelfTo<ShipController>().AsSingle();
            Container.BindInterfacesAndSelfTo<ShipView>().FromInstance(_viewShip).AsSingle();

            Container.BindInterfacesAndSelfTo<ShipAntigravityController>().AsSingle();
            Container.BindInterfacesAndSelfTo<ShipAntigravityView>().FromInstance(_viewShipAntigravity).AsSingle();
            
            //UI
            Container.BindInterfacesAndSelfTo<PlayerInteractorView>().FromInstance(_viewPlayerInter).AsSingle();

            
            Scene scene = SceneManager.GetActiveScene();
            var objects = scene.GetRootGameObjects();
            foreach (GameObject obj in objects)
            {
                var splineTriggers = obj.GetComponentsInChildren<SplineTrigger>(true);
                foreach (SplineTrigger trigger in splineTriggers)
                {
                    Container.BindInterfacesAndSelfTo<SplineTrigger>().FromInstance(trigger).AsTransient();
                }
            }
        }
    }
}

