﻿using GameSystems.Ship.ShipEngine;
using UnityEngine;
using Zenject;

namespace _Init
{
    public class ShipInstaller : MonoInstaller
    {
        [Header("Ship Engine")]
        [SerializeField] private ShipEngineView _viewShipEngine;
        [SerializeField] private ShipEngineSubviewPiece _subviewPiece;
        [SerializeField] private ShipEngineSubviewTumbler _subviewTumbler;
        [SerializeField] private ShipEngineSubviewFire _subviewFire;
        [SerializeField] private ShipEngineSubviewProtection _subviewProtection;
        [SerializeField] private ShipEngineSubviewSpeedLever _subviewSpeed;
        [SerializeField] private ShipEngineSubviewRecharger _subviewRecharger;
        
        public override void InstallBindings()
        {
            var shipEngine = new ShipEngineController(_viewShipEngine);
            Container.BindInterfacesAndSelfTo<ShipEngineController>().FromInstance(shipEngine).AsSingle();
            
            Container.BindInterfacesAndSelfTo<ShipEngineSubviewPiece>().FromInstance(_subviewPiece).AsTransient();
            Container.BindInterfacesAndSelfTo<ShipEngineSubviewSpeedLever>().FromInstance(_subviewSpeed).AsSingle();
            Container.BindInterfacesAndSelfTo<ShipEngineSubviewProtection>().FromInstance(_subviewProtection).AsSingle();
            Container.BindInterfacesAndSelfTo<ShipEngineSubviewTumbler>().FromInstance(_subviewTumbler).AsSingle();
            Container.BindInterfacesAndSelfTo<ShipEngineSubviewFire>().FromInstance(_subviewFire).AsSingle();
            Container.BindInterfacesAndSelfTo<ShipEngineSubviewRecharger>().FromInstance(_subviewRecharger).AsSingle();
            
            Container.BindInterfacesAndSelfTo<ShipEngineView>().FromInstance(_viewShipEngine).AsSingle();
            
            
        }
    }
}