﻿using BL.KoalaLogic.Check;
using BL.KoalaLogic.LogicBlock;
using BL.LogicBlock.NodeBased.Addons;
using BL.Services.LogicSequences;
using BLLogic.Nodes;
using Runtime.LogicBlock.NodeBased.Service;
using UnityEngine;
using UnityEngine.SceneManagement;
using XNode;
using Zenject;
using ZenjectUtil.Test.Extensions;

namespace BL.Installers
{
    public class LogicBlockInstaller : MonoInstaller
    {

        public override void InstallBindings()
        {
            Scene scene = SceneManager.GetActiveScene();
            var sceneRoots = scene.GetRootGameObjects();

            Container.BindSubstituteInterfacesTo<ILogicSequenceService, LogicSequenceService>().AsSingle();
            Container.BindSubstituteInterfacesTo<ISceneNodeContextService, SceneNodeContextService>().AsSingle();

            foreach (GameObject root in sceneRoots)
            {
                SetLogic(root);
            }
        }

        private void SetLogic(GameObject root)
        {
            var logicContext = root.GetComponentsInChildren<LogicContext>(true);
            foreach (LogicContext node in logicContext)
            {
                Container.BindInterfacesAndSelfTo<ILogicContext>().FromInstance(node).AsTransient();
            }

            var logicChecks = root.GetComponentsInChildren<LogicCheckBase>(true);
            foreach (LogicCheckBase check in logicChecks)
            {
                Container.BindInterfacesTo<ILogicCheck>().FromInstance(check).AsTransient();
            }

            var sequences = root.GetComponentsInChildren<LogicSequence>(true);
            foreach (LogicSequence seq in sequences)
            {
                Container.BindInterfacesAndSelfTo<LogicSequence>().FromInstance(seq).AsTransient();
                if (!seq.Graph) continue;
                seq.Graph.InitData();
                foreach (Node nodeRaw in seq.Graph.nodes)
                {
                    if (!(nodeRaw is LogicNodeAbstract node)) continue;

                    node = (LogicNodeAbstract) nodeRaw;
                    Container.QueueForInject(node);

                    if (node is ILogicTickableNode)
                    {
                        Container.BindInterfacesAndSelfTo<ILogicTickableNode>().FromInstance(node).AsTransient();
                    }
                    else
                    {
                        Container.BindInterfacesAndSelfTo<ILogicNode>().FromInstance(node).AsTransient();
                    }
                }
            }
        }
    }
}