﻿using AWShip.Input.Interaction;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace UI
{
    public sealed class PlayerInteractorView : MonoBehaviour
    {
        [SerializeField] private TMP_Text _firstLine;
        [SerializeField] private TMP_Text _secondLine;
        [SerializeField] private float _timeToUpdate = 0.2f;
        
        [SerializeField] private Color _colorShown = Color.white;

        public void NoViewForced()
        {
            _firstLine.color = Color.clear;
            _firstLine.gameObject.SetActive(false);
            _secondLine.color = Color.clear;
            _secondLine.gameObject.SetActive(false);
        }
        
        public void NoView()
        {
            HideLine(_firstLine);
            HideLine(_secondLine);
        }
        private void OnlyOneView(IInteractable target, bool isLeft)
        {
            if (_firstLine.gameObject.activeInHierarchy)
            {
                UpdateLine(_firstLine, target, isLeft);
            }
            else
            {
                ShowLine(_firstLine, target, isLeft);
            }
            
            HideLine(_secondLine);
        }

        private void BothView(IInteractableLeft first, IInteractableRight second)
        {
            if (_firstLine.gameObject.activeInHierarchy)
            {
                UpdateLine(_firstLine, first, true);
            }
            else
            {
                ShowLine(_firstLine, first, true);
            }
            if (_secondLine.gameObject.activeInHierarchy)
            {
                UpdateLine(_secondLine, second, false);
            }
            else
            {
                ShowLine(_secondLine, second, false);
            }
        }

        public void SetView(IInteractableLeft first, IInteractableRight second)
        {
            if (first == null && second == null)
            {
                NoView();
            }
            else if(first != null && second == null)
            {
                OnlyOneView(first, true);
            }
            else if(first == null)
            {
                OnlyOneView(second, false);
            }
            else
            {
                BothView(first, second);
            }
        }

        private void UpdateLine(TMP_Text text, IInteractable inter, bool isLeft)
        {
            text.DOKill();
            text.text = inter.GetLocalizedString(isLeft);
            text.color = _colorShown;
            text.gameObject.SetActive(true);
        }
        
        private void ShowLine(TMP_Text text, IInteractable inter, bool isLeft)
        {
            text.DOKill();
            text.text = inter.GetLocalizedString(isLeft);
            text.color = Color.clear;
            text.gameObject.SetActive(true);
            text.DOColor(_colorShown, _timeToUpdate);
        }

        private void HideLine(TMP_Text text)
        {
            text.DOKill();
            text.DOColor(Color.clear, _timeToUpdate).OnComplete(() => text.gameObject.SetActive(false));
        }
    }
}