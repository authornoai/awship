﻿using BL.KoalaLogic.LogicBlock;
using GameSystems.Ship.ShipCore;
using UnityEngine;
using Zenject;

namespace GameSystems.Path
{
    public class SplineTrigger : MonoBehaviour, ITickable, IInitializable
    {
        [SerializeField] private BezierSpline _spline;
        [SerializeField] private float _position;
        [SerializeField] private LogicSequence _target;

        [Inject] private ShipView _ship;

        private bool _isTick = true;
        private bool _isTriggered;
        private Vector3 _realPos;

        private static float _splineOnStep = 500.0f;
        private static float _splineTrigger = 2.0f * 2.0f;

        private void OnDrawGizmos()
        {
            if(!_spline) return;
            Gizmos.color = Color.magenta;
            Gizmos.DrawSphere(_spline.GetPointLenght(_position * _splineOnStep), 0.25f);
        }

        public void Tick()
        {
            if(!_isTick || _isTriggered) return;

            Vector3 shipPos = _ship.GetShipPosition();

            if ((shipPos - _realPos).sqrMagnitude > _splineTrigger) return;
            
            Debug.Log("Trigger");
            _isTriggered = true;
            _target.Launch(_ship.gameObject, gameObject, true);
        }

        public void Initialize()
        {
            _realPos = _spline.GetPointLenght(_position * _splineOnStep);
        }
    }
}