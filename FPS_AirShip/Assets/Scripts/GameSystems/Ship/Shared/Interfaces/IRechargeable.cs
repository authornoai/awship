﻿namespace GameSystems.Ship.Shared.Interfaces
{
    public interface IRechargeable
    {
        void Recharge();
    }
}