﻿using DG.Tweening;
using UnityEngine;

namespace GameSystems.Ship.Shared
{
    public abstract class SubviewPiece : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer _spriteRenderer;

        public void SetColor(Color colorGoodValue) => _spriteRenderer.color = colorGoodValue;
        public void ChangeColor(Color toSet, float time) => _spriteRenderer.DOColor(toSet, time);

        public void ChangeColorByValue(float coefficient, Color goodColor, Color badColor, float time) =>
            _spriteRenderer.DOColor(Color.Lerp(goodColor, badColor, coefficient), time);

        public void SetColorAndScaleByValue(float coefficient, float defaultScale, float maxScale,
            Color goodColor, Color badColor, Vector3 offsetMin, Vector3 offsetMax)
        {
            _spriteRenderer.color = Color.Lerp(goodColor, badColor, coefficient);

            Vector3 currentScale = _spriteRenderer.transform.localScale;
            currentScale.y = Mathf.Lerp(defaultScale, maxScale, coefficient);
            _spriteRenderer.transform.localScale = currentScale;

            _spriteRenderer.transform.localPosition = Vector3.Lerp(offsetMin, offsetMax, coefficient);
        }

        public void ChangeScaleByValue(float coefficient, float defaultScale, float maxScale,
            Vector3 offsetMin, Vector3 offsetMax, float time)
        {
            Vector3 newScale = _spriteRenderer.transform.localScale;
            newScale.y = Mathf.Lerp(defaultScale, maxScale, coefficient);
            _spriteRenderer.transform.DOScale(newScale, time);

            _spriteRenderer.transform.DOLocalMove
                (Vector3.Lerp(offsetMin, offsetMax, coefficient), time);
        }
    }
}