﻿using AWShip.Input.Interaction;
using GameSystems.Ship.Shared.Interfaces;
using TMPro;
using UnityEngine;
using Zenject;

namespace GameSystems.Ship.ShipAntigravity.Subviews
{
    public abstract class SubviewRecharger<T> : MonoBehaviour, IInteractableLeft 
        where T : IRechargeable
    {
        [SerializeField] private TMP_Text _rechargeText;
        [SerializeField] private Collider _collider;

        [Inject] private T _controller;
        
        public void OnInteract_Start(bool isLeft)
        {
            _controller.Recharge();
        }

        public void OnInteract_End(bool isLeft) { }

        //TODO: Link
        public string GetLocalizedString(bool isLeft) => "Recharge";
        public void SetInteractable(bool isInteractable) => _collider.enabled = isInteractable;

        public void SetStorage(int toSet) => _rechargeText.text = toSet.ToString();
    }
}