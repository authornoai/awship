﻿using GameSystems.Ship.Shared.Interfaces;
using GameSystems.Ship.ShipCore;
using UnityEngine;
using Zenject;

namespace GameSystems.Ship.ShipAntigravity
{
    public sealed class ShipAntigravityController : IInitializable, IRechargeable
    {
        private ShipAntigravityModel _model;

        private readonly ShipAntigravityView _view;
        
        public ShipAntigravityController(ShipAntigravityView view)
        {
            _view = view;
        }
        
        public void Initialize()
        {
            _model = new ShipAntigravityModel()
            {
                
                AntigravityStorageMax = 3,
                AntigravityStorageMin = 0,
                
                AntigravityMin = 0,
                AntigravityMax = 5,
                AntigravityUse = 1,
                AntigravityRecharge = 3,
                
                CurrentAntigravityStorage = 3,
                CurrentAntigravity = 5
            };
        }
        
        public void Tick(float heightCoefficient)
        {
            _view.UpdateAntigravity(_model.CurrentAntigravity / _model.AntigravityMax, heightCoefficient);
        }

        public bool HasAntigravity() => _model.CurrentAntigravity > _model.AntigravityMin;
        public void SpendAntigravityTick()
        {
            _model.CurrentAntigravity -= _model.AntigravityUse * Time.deltaTime;
        }

        private bool CanRecharge() => _model.CurrentAntigravityStorage > _model.AntigravityStorageMin;
        public void Recharge()
        {
            if (!CanRecharge() 
                && Mathf.Abs(_model.CurrentAntigravity - _model.AntigravityMax) > 0.05f) return;
            
            --_model.CurrentAntigravityStorage;
            _model.CurrentAntigravity = Mathf.Clamp(_model.CurrentAntigravity + _model.AntigravityRecharge,
                _model.AntigravityMin, _model.AntigravityMax);
           
            if (!CanRecharge())
            {
                _view.SetRechargerInteractable(false);
            }

            _view.SetRechargerStorage(_model.CurrentAntigravityStorage);
            
        }

        
    }
}