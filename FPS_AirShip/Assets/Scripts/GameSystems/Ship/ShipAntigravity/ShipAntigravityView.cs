﻿using GameSystems.Ship.ShipAntigravity.Subviews;
using UnityEngine;
using Zenject;

namespace GameSystems.Ship.ShipAntigravity
{
    public class ShipAntigravityView : MonoBehaviour, IInitializable
    {
        [SerializeField] private ShipAntigravitySubviewHeightButton[] _buttons;
        
        [Header("Bar")]
        [SerializeField] private ShipAntigravitySubviewPower _power;
        [SerializeField] private float _powerScaleMin;
        [SerializeField] private float _powerScaleMax;
        [SerializeField] private Color _powerColor = Color.blue;
        [SerializeField] private Vector3 _powerOffsetMin = Vector3.zero;
        [SerializeField] private Vector3 _powerOffsetMax = Vector3.zero;

        [Header("Recharger")] 
        [SerializeField] private ShipAntigravitySubviewRecharger _recharger;

        [Header("Radar")]
        [SerializeField] private ShipAntigravitySubviewHeightRadar _radar;

        [Inject] private DiContainer _container;
        
        public void Initialize()
        {
            foreach (ShipAntigravitySubviewHeightButton button in _buttons)
            {
                _container.BindInterfacesAndSelfTo<ShipAntigravitySubviewHeightButton>()
                    .FromInstance(button)
                    .AsTransient();
            }
        }

        public void UpdateAntigravity(float antigravityPercentage, float heightCoefficient)
        {
            _power.SetColorAndScaleByValue(antigravityPercentage, _powerScaleMin, _powerScaleMax,
                _powerColor, _powerColor, _powerOffsetMin, _powerOffsetMax);
            
            _radar.UpdateRadar(heightCoefficient);
        }

        public void SetRechargerInteractable(bool isInteractable) => _recharger.SetInteractable(isInteractable);
        public void SetRechargerStorage(int toSet) => _recharger.SetStorage(toSet);
    }
}