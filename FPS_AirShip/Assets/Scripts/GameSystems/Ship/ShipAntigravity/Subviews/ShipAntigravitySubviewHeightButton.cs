﻿using AWShip.Input.Interaction;
using GameSystems.Ship.ShipCore;
using UnityEngine;
using Zenject;

namespace GameSystems.Ship.ShipAntigravity.Subviews
{
    public class ShipAntigravitySubviewHeightButton : MonoBehaviour, IInteractableLeft
    {
        [SerializeField] private bool _isUp = true;
        [SerializeField] private string _localizedString = "";

        [Inject] private ShipController _controller;

        public void OnInteract_Start(bool isLeft) => _controller.StepHeight(_isUp);
        public void OnInteract_End(bool isLeft) =>  _controller.StopHeight();

        //TODO: Add loca
        public string GetLocalizedString(bool isLeft) => _localizedString;
    }
}