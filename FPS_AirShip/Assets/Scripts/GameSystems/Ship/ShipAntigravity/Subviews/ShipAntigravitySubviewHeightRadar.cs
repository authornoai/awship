﻿using UnityEngine;

namespace GameSystems.Ship.ShipAntigravity.Subviews
{
    public class ShipAntigravitySubviewHeightRadar : MonoBehaviour
    {
        [SerializeField] private Vector3 _offsetMin;
        [SerializeField] private Vector3 _offsetMax;
        [SerializeField] private SpriteRenderer _target;

        public void UpdateRadar(float coefficient)
        {
            _target.transform.localPosition = Vector3.Lerp(_offsetMin, _offsetMax, coefficient);
        }
    }
}