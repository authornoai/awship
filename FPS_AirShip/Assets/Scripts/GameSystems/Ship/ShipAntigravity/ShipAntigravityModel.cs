﻿namespace GameSystems.Ship.ShipAntigravity
{
    public struct ShipAntigravityModel
    {
        public float AntigravityMin;
        public float AntigravityMax;
        public float AntigravityUse;
        public float AntigravityRecharge;
        
        public int AntigravityStorageMax;
        public int AntigravityStorageMin;
        
        public float CurrentAntigravity;
        public int CurrentAntigravityStorage;
    }
}