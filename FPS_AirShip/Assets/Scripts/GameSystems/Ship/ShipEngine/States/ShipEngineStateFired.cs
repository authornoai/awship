﻿using AWShip.Core.StateMachine;

namespace GameSystems.Ship.ShipEngine.States
{
    public sealed class ShipEngineStateFired : IFsmState
    {
        private ShipEngineController _controller;

        public ShipEngineStateFired(ShipEngineController controller)
        {
            _controller = controller;
        }


        public void OnEnter() => _controller.State_OnFired();
        
        public void OnUpdate() { }
        public void OnExit() { }
    }
}