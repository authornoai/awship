﻿using AWShip.Core.StateMachine;

namespace GameSystems.Ship.ShipEngine.States
{
    public sealed class ShipEngineStateWorking : IFsmState
    {
        private readonly ShipEngineController _controller;

        public ShipEngineStateWorking(ShipEngineController controller)
        {
            _controller = controller;
        }

        public void OnEnter() => _controller.State_OnWorking();
        public void OnUpdate() => _controller.State_Working_UpdateWorking();
        public void OnExit() { }
    }
}