﻿using AWShip.Core.StateMachine;
using UnityEngine;

namespace GameSystems.Ship.ShipEngine.States
{
    public sealed class ShipEngineStateTurnedOff : IFsmState
    {
        private ShipEngineController _controller;
        public ShipEngineStateTurnedOff(ShipEngineController controller)
        {
            _controller = controller;
        }

        public void OnEnter()
        {
            _controller.State_OnTurnedOff();
        }
        public void OnUpdate() { }
        public void OnExit() { }
        
    }
}