﻿using AWShip.Input.Interaction;
using UnityEngine;
using Zenject;

namespace GameSystems.Ship.ShipEngine
{
    public sealed class ShipEngineSubviewTumbler : MonoBehaviour, IInteractableLeft
    {
        [Inject] private ShipEngineController _controller;

        [SerializeField] private Collider _collider;
        [SerializeField] private MeshRenderer _mesh;
        [SerializeField] private Material _matTurnedOn;
        [SerializeField] private Material _matTurnedOff;

        public void DisableTumbler()
        {
            _mesh.sharedMaterial = _matTurnedOff;
            _collider.enabled = false;
        }

        public void EnableTumbler()
        {
            _collider.enabled = true;
        }
        
        public void OnInteract_Start(bool isLeft)
        {

            _controller.Inter_OnTurnTumbler();
            EShipEngineStatus status = _controller.GetEngineStatus();
            
            _mesh.sharedMaterial = status switch
            {
                EShipEngineStatus.Working => _matTurnedOn,
                EShipEngineStatus.TurnedOff => _matTurnedOff,
                _ => _mesh.material
            };
        }

        public void OnInteract_End(bool isLeft) { }

        //TODO: Normal link
        public string GetLocalizedString(bool isLeft)
        {
            return _mesh.sharedMaterial == _matTurnedOn ? "Turn Off" : "Turn On";
        }
    }
}