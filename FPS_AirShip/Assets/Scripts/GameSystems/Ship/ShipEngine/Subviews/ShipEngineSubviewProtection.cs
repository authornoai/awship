﻿using System;
using DG.Tweening;
using UnityEngine;

namespace GameSystems.Ship.ShipEngine
{
    public sealed class ShipEngineSubviewProtection : MonoBehaviour
    {
        [Header("Links")] 
        [SerializeField] private Transform _toScale;
        [SerializeField] private SpriteRenderer _toColor;

        [Header("Math")]
        [SerializeField] private Vector3 _scaleDefault;
        [SerializeField] private Vector3 _scaleMin;
        [SerializeField] private Vector3 _offsetDefault;
        [SerializeField] private Vector3 _offsetMin;

        public void Tick(float coefficient, Color colorMin, Color colorMax)
        {
            _toScale.localScale = Vector3.Lerp(_scaleMin, _scaleDefault, coefficient);
            _toScale.localPosition = Vector3.Lerp(_offsetMin, _offsetDefault, coefficient);
            _toColor.color = Color.Lerp(colorMin, colorMax, coefficient);
        }

        public void Change(float coefficient, float time, Action onEnd, Color colorMin, Color colorMax)
        {
            _toScale.DOScale(Vector3.Lerp(_scaleMin, _scaleDefault, coefficient), time).OnComplete(() => onEnd?.Invoke());
            _toScale.DOLocalMove(Vector3.Lerp(_offsetMin, _offsetDefault, coefficient), time);
            _toColor.DOColor(Color.Lerp(colorMin, colorMax, coefficient), time);
        }
    }
}