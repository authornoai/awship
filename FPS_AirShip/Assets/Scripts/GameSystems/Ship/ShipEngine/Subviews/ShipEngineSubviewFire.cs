﻿using AWShip.Input.Interaction;
using UnityEngine;
using Zenject;

namespace GameSystems.Ship.ShipEngine
{
    public sealed class ShipEngineSubviewFire : MonoBehaviour, IInteractableLeft
    {
        [Inject] private ShipEngineController _controller;
        
        [SerializeField] private SpriteRenderer _fire;
        [SerializeField] private Collider _target;

        [SerializeField] private int _firePowerMax = 5;
        private int _firePowerCurrent;
        
        public void SetFire(bool active)
        {
            _fire.gameObject.SetActive(active);
            if (active)
            {
                _firePowerCurrent = _firePowerMax;
                _target.enabled = true;
            }
            else
            {
                _target.enabled = false;
            }
        }

        public void OnInteract_Start(bool isLeft)
        {
            --_firePowerCurrent;
            if (_firePowerCurrent > 0) return;
            SetFire(false);
            _controller.Inter_OnFireRemoved();
        }

        public void OnInteract_End(bool isLeft) { }

        //TODO: Normal link
        public string GetLocalizedString(bool isLeft) => "Put Out";
    }
}