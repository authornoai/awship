﻿using AWShip.Input.Interaction;
using GameSystems.Ship.ShipCore;
using UnityEngine;
using Zenject;

namespace GameSystems.Ship.ShipEngine
{
    public class ShipEngineSubviewSpeedLever : MonoBehaviour, IInteractableLeft, IInteractableRight
    {
        [SerializeField] private Collider _collider;
        [SerializeField] private MeshRenderer _mesh;
        [SerializeField] private Material[] _matSpeedLevel;
        [SerializeField] private Material _matOnDisabled;

        [Inject] private ShipEngineController _engineController;
        [Inject] private ShipController _controller;

        
        public void SetInteractable(bool isEnabled)
        {
            if (!isEnabled)
            {
                _controller.ResetForwardSpeed();
            }
            
            _collider.enabled = isEnabled;
            _mesh.sharedMaterial = isEnabled ? 
                _matSpeedLevel[_controller.GetSpeedMultiplier()] : 
                _matOnDisabled;
        }
        
        public void OnInteract_Start(bool isLeft)
        {
            if(_engineController.GetEngineStatus() != EShipEngineStatus.Working) return;

            if (isLeft)
            {
                _controller.StepForwardSpeed(1);
            }
            else
            {
                _controller.StepForwardSpeed(-1);
            }
            _mesh.sharedMaterial = _matSpeedLevel[_controller.GetSpeedMultiplier()];
        }

        public void OnInteract_End(bool isLeft) { }

        public string GetLocalizedString(bool isLeft)
        {
            return isLeft ? "Increase Speed" : "Decrease Speed";
        }
    }
}