﻿namespace GameSystems.Ship.ShipEngine
{
    public struct ShipEngineModel
    {
        public EShipEngineStatus Status;
        
        public float CurrentPiece;
        public float CurrentPieceChangePerSecond;
        public float CurrentProtection;
        public int CurrentRecharger;
        
        public float PieceChangePerSecondDefault;
        public float PieceChangePerFix;
        public float PieceMax;
        public float PieceDefault;
        public int PieceStartSpeedMultiToChange;

        public int RechargerMin;
        public int RechargerMax;
        
        public float ProtectionDefault;
        public float ProtectionRepairPerSecond;
        
        public bool WasBroken;
    }
}