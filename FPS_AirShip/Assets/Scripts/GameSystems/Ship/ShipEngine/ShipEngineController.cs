﻿using AWShip.Core.StateMachine;
using GameSystems.Ship.Shared.Interfaces;
using GameSystems.Ship.ShipEngine.States;
using UnityEngine;
using Zenject;

namespace GameSystems.Ship.ShipEngine
{
    public sealed class ShipEngineController : IInitializable,
        ITickable, IRechargeable
    {
        private ShipEngineModel _model;
        private readonly ShipEngineView _view;
        private FsmBase _shipEngineFsm;

        private bool _readyToUpdate;

        public ShipEngineController(ShipEngineView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _model = new ShipEngineModel
            {
                Status = EShipEngineStatus.TurnedOff,

                WasBroken = false,
                CurrentProtection = 4,
                ProtectionDefault = 4,
                ProtectionRepairPerSecond = 0.1f,

                CurrentPiece = 0.5f,
                CurrentPieceChangePerSecond = 0.05f,

                PieceStartSpeedMultiToChange = 2,
                PieceChangePerSecondDefault = 0.05f,
                PieceChangePerFix = 0.6f,
                PieceDefault = 0,
                PieceMax = 4,

                CurrentRecharger = 3,
                RechargerMin = 0,
                RechargerMax = 3
            };

            var stateTurnedOff = new ShipEngineStateTurnedOff(this);
            var stateWorking = new ShipEngineStateWorking(this);
            var stateFired = new ShipEngineStateFired(this);

            _shipEngineFsm = new FsmBase(stateTurnedOff, 4);
            _shipEngineFsm.AddTransition(stateTurnedOff, stateWorking,
                () => _model.Status == EShipEngineStatus.Working,
                () => _model.Status == EShipEngineStatus.TurnedOff);
            _shipEngineFsm.AddTransition(stateWorking, stateFired,
                () => _model.Status == EShipEngineStatus.Fired);
            _shipEngineFsm.AddTransition(stateFired, stateTurnedOff,
                () => _model.Status == EShipEngineStatus.TurnedOff);
        }

        public void Tick() => _shipEngineFsm.Update();
        public EShipEngineStatus GetEngineStatus() => _model.Status;

        public void SetSpeedMultiplier(int speedMulti)
        {
            if (speedMulti < _model.PieceStartSpeedMultiToChange)
            {
                _model.CurrentPieceChangePerSecond = 0.0f;
            }
            else
            {
                _model.CurrentPieceChangePerSecond = _model.PieceChangePerSecondDefault *
                                                     (speedMulti - _model.PieceStartSpeedMultiToChange + 1);
            }
        }

        #region STATE_TURNED_OFF

        public void State_OnTurnedOff()
        {
            _view.State_OnTurnedOff(_model.CurrentProtection / _model.ProtectionDefault);
            _readyToUpdate = false;
        }

        public void Inter_OnTurnTumbler()
        {
            _model.Status = _model.Status switch
            {
                EShipEngineStatus.TurnedOff => EShipEngineStatus.Working,
                EShipEngineStatus.Working => EShipEngineStatus.TurnedOff,
                _ => _model.Status
            };
        }

        #endregion

        #region STATE_WORKING

        public void State_OnWorking()
        {
            if (_model.WasBroken)
            {
                _model.WasBroken = false;
                _model.CurrentProtection = _model.ProtectionDefault;
                _model.CurrentPiece = _model.PieceDefault;

                _view.ChangeProtectionBlocked(_model.CurrentProtection / _model.ProtectionDefault,
                    () =>
                    {
                        _readyToUpdate = true;
                        _view.State_OnWorking(_model.CurrentPiece, _model.PieceDefault, _model.PieceMax);
                    });
                _view.ChangePiecesScale(_model.CurrentPiece, _model.PieceDefault, _model.PieceMax);
            }
            else
            {
                _readyToUpdate = true;
                _view.State_OnWorking(_model.CurrentPiece, _model.PieceDefault, _model.PieceMax);
            }
        }

        public void State_Working_UpdateWorking()
        {
            if (!_readyToUpdate) return;

            bool allFine = true;
            var deltaChange = _model.CurrentPieceChangePerSecond * Time.deltaTime;
            _model.CurrentPiece += deltaChange;

            if (_model.CurrentPiece > _model.PieceMax - 0.02f)
            {
                allFine = false;
                _model.CurrentPiece = _model.PieceMax;
                _model.CurrentProtection -= deltaChange;
            }

            if (allFine)
            {
                _model.CurrentProtection += _model.ProtectionRepairPerSecond * Time.deltaTime;
                if (_model.CurrentProtection > _model.ProtectionDefault)
                {
                    _model.CurrentProtection = _model.ProtectionDefault;
                }
            }

            if (_model.CurrentProtection < 0)
            {
                _model.Status = EShipEngineStatus.Fired;
                _model.CurrentProtection = 0;
            }

            _view.TickValues(_model.CurrentPiece, 0, _model.PieceMax);
            _view.TickProtection(_model.CurrentProtection / _model.ProtectionDefault);
        }

        private void Inter_OnRechargeAttempt()
        {
            if (_model.Status != EShipEngineStatus.Working || !CanRecharge()
                                                           || _model.CurrentPiece < _model.PieceDefault + 0.05f) return;

            --_model.CurrentRecharger;
            _model.CurrentPiece -= _model.PieceChangePerFix;
            if (_model.CurrentPiece < _model.PieceDefault)
            {
                _model.CurrentPiece = _model.PieceDefault;
            }

            if (!CanRecharge())
            {
                _view.SetRechargerInteractable(false);
            }

            _view.SetRechargerStorage(_model.CurrentRecharger);
        }

        #endregion

        #region STATE_FIRE

        public void State_OnFired()
        {
            _model.WasBroken = true;
            _view.State_OnFired(_model.CurrentProtection / _model.ProtectionDefault);
        }

        public void Inter_OnFireRemoved()
        {
            _model.Status = EShipEngineStatus.TurnedOff;
        }

        #endregion

        private bool CanRecharge() => _model.CurrentRecharger > _model.RechargerMin;
        public void Recharge() => Inter_OnRechargeAttempt();
    }
}