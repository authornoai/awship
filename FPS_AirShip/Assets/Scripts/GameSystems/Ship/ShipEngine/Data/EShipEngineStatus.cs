﻿namespace GameSystems.Ship.ShipEngine
{
    public enum EShipEngineStatus
    {
        TurnedOff,
        Working,
        Fired,
    }
}