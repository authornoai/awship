﻿using System;
using UnityEngine;
using Zenject;

namespace GameSystems.Ship.ShipEngine
{
    public sealed class ShipEngineView : MonoBehaviour, IInitializable
    {
        [Header("Subviews")]
        [Inject] private ShipEngineSubviewPiece _subviewPiece;
        [Inject] private ShipEngineSubviewTumbler _subviewTumbler;
        [Inject] private ShipEngineSubviewFire _subviewFire;
        [Inject] private ShipEngineSubviewProtection _subviewProtection;
        [Inject] private ShipEngineSubviewSpeedLever _subviewSpeed;
        [Inject] private ShipEngineSubviewRecharger _subviewRecharger;
        
        [Header("Colors")]
        [SerializeField] private Color _colorGoodValue = Color.green;
        [SerializeField] private Color _colorBadValue = Color.red;
        [SerializeField] private Color _colorBlockedValue = Color.black;

        [Header("Piece Scales")] 
        [SerializeField] private float _scaleDefault = 1.0f;
        [SerializeField] private float _scaleMax = 1.75f;
        [SerializeField] private Vector3 _offsetMin = Vector3.zero;
        [SerializeField] private Vector3 _offsetMax = Vector3.up;

        [Header("Time")]
        [SerializeField] private float _timeToChangeState = 0.125f;
        [SerializeField] private float _timeToRepair = 1.0f;

        public void Initialize()
        {
            _subviewPiece.SetColor(_colorGoodValue);

            _subviewProtection.Change(1.0f, _timeToRepair, null, _colorBlockedValue, _colorBlockedValue);
            _subviewFire.SetFire(false);
            _subviewSpeed.SetInteractable(false);
        }

        public void TickValues(float piece, float pieceDefault, float pieceMax)
        {
            float pieceCoefficient = piece / (pieceMax + pieceDefault);
            _subviewPiece.SetColorAndScaleByValue(pieceCoefficient, 
                _scaleDefault, _scaleMax,
                _colorGoodValue, _colorBadValue,
                _offsetMin, _offsetMax);
        }
        public void ChangePiecesScale(float piece, float pieceDefault, float pieceMax)
        {
            float pieceCoefficient = piece / (pieceMax + pieceDefault);
            _subviewPiece.ChangeScaleByValue(pieceCoefficient, 
                _scaleDefault, _scaleMax, _offsetMin, _offsetMax, _timeToRepair);
        }
        public void TickProtection(float coefficient) 
            => _subviewProtection.Tick(coefficient, _colorBadValue, _colorGoodValue);
        public void ChangeProtectionBlocked(float coefficient, Action onEnd) 
            => _subviewProtection.Change(coefficient, _timeToRepair, onEnd, _colorBlockedValue, _colorBlockedValue);

        public void State_OnTurnedOff(float protectionCoefficient)
        {
            _subviewPiece.ChangeColor(_colorBlockedValue, _timeToChangeState);
            
            _subviewProtection.Change(protectionCoefficient, _timeToChangeState, null, _colorBlockedValue, _colorBlockedValue);
            _subviewTumbler.EnableTumbler();
            _subviewSpeed.SetInteractable(false);
        }
        
        public void State_OnWorking(float piece, float pieceDefault, float pieceMax)
        {
            float coefficient = piece / (pieceMax + pieceDefault);
            _subviewPiece.ChangeColorByValue(coefficient, _colorGoodValue, _colorBadValue, _timeToChangeState);
            
            _subviewSpeed.SetInteractable(true);
        }

        public void State_OnFired(float protectionCoefficient)
        {
            _subviewPiece.ChangeColor(_colorBlockedValue, _timeToChangeState);
            
            _subviewFire.SetFire(true);
            _subviewProtection.Change(protectionCoefficient, _timeToChangeState, null, _colorBlockedValue, _colorBlockedValue);
            _subviewTumbler.DisableTumbler();
            _subviewSpeed.SetInteractable(false);
        }

        public void SetRechargerInteractable(bool setActive) => _subviewRecharger.SetInteractable(setActive);
        public void SetRechargerStorage(int toSet) => _subviewRecharger.SetStorage(toSet);
    }
}