﻿using UnityEngine;
using Zenject;

namespace GameSystems.Ship.ShipCore
{
    public sealed class ShipView : MonoBehaviour, IInitializable
    {
        [SerializeField] private Transform _mainTransform;
        [SerializeField] private BezierSpline _splineToFollow;
        [SerializeField] private float _step = 1000f;

        private bool _needY;
        private float _speedY;

        private float _speedFwd;
        
        private Vector3 _toGetPoint;
        
        private float _splineLenght;

        public void Initialize()
        {
            _splineLenght = 0.0f;
            
            _toGetPoint = _splineToFollow.GetPointLenght(_splineLenght);
            _mainTransform.position = _toGetPoint;
        }

        public bool IsSpendingAntigravity() => _needY;
        public bool IsMovingUp() => _speedY > 0.0f;

        public void TickPosition(bool hasFwd, bool hasAntigravity)
        {
            Vector3 deltaMove = Vector3.zero;

            if (hasFwd)
            {
                if (Mathf.Abs(_speedFwd) > 0 && (_toGetPoint - _mainTransform.position).sqrMagnitude < 100)
                {
                    _splineLenght += _step;
                    _toGetPoint = _splineToFollow.GetPointLenght(_splineLenght);
                }
                
                deltaMove = Vector3.MoveTowards(_mainTransform.position, _toGetPoint, _speedFwd * Time.fixedDeltaTime) - _mainTransform.position;
                deltaMove.y = 0;
            }
            
            if (_needY && hasAntigravity) deltaMove += _speedY * Time.fixedDeltaTime * Vector3.up;
            
            _mainTransform.localPosition += deltaMove;
        }

        public void SetForwardSpeed(float speedFwd) => _speedFwd = speedFwd;

        public void SetHeightSpeed(float speedHeight)
        {
            _needY = true;
            _speedY = speedHeight;
        }

        public void StopHeightSpeed() => _needY = false;

        public Vector3 GetShipPosition() => _mainTransform.localPosition;
    }
}