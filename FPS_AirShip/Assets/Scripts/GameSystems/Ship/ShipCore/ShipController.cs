﻿using GameSystems.Ship.ShipAntigravity;
using GameSystems.Ship.ShipEngine;
using UnityEngine;
using Zenject;

namespace GameSystems.Ship.ShipCore
{
    public sealed class ShipController : IInitializable, IFixedTickable, ITickable
    {
        private readonly ShipView _view;
        private readonly ShipEngineController _controllerEngine;
        private readonly ShipAntigravityController _controllerAntigravity;
        
        private ShipModel _model;

        public ShipController(ShipView view, 
            ShipEngineController controllerEngine,
            ShipAntigravityController controllerAntigravity)
        {
            _view = view;
            _controllerEngine = controllerEngine;
            _controllerAntigravity = controllerAntigravity;
        }
        
        public void Initialize()
        {
            _model = new ShipModel()
            {
                HeightChangeSpeed = 2.5f,
                HeightMin = -2.5f,
                HeightMax = 2.5f,

                SpeedMultiplierMin = 0,
                SpeedMultiplierMax = 4,
                SpeedStep = 5f,
                
                CurrentSpeed = 0,
                CurrentSpeedMultiplier = 0
            }; 
        }

        public void ResetForwardSpeed()
        {
            _model.CurrentSpeedMultiplier = _model.SpeedMultiplierMin;
            _model.CurrentSpeed = _model.CurrentSpeedMultiplier * _model.SpeedStep;
            
            _controllerEngine.SetSpeedMultiplier(_model.CurrentSpeedMultiplier);
            _view.SetForwardSpeed(_model.CurrentSpeed);
        }
        public void StepForwardSpeed(int deltaSetMulti)
        {
            _model.CurrentSpeedMultiplier = Mathf.Clamp(_model.CurrentSpeedMultiplier + deltaSetMulti,
                _model.SpeedMultiplierMin, _model.SpeedMultiplierMax);
            _model.CurrentSpeed = _model.CurrentSpeedMultiplier * _model.SpeedStep;
            
            _controllerEngine.SetSpeedMultiplier(_model.CurrentSpeedMultiplier);
            _view.SetForwardSpeed(_model.CurrentSpeed);
        }

        public void StepHeight(bool isUp) => _view.SetHeightSpeed(isUp ? 1 : -1 * _model.HeightChangeSpeed);
        public void StopHeight() => _view.StopHeightSpeed();

        public void FixedTick()
        {
            if(!_controllerAntigravity.HasAntigravity()) _view.StopHeightSpeed();
            if (_view.IsSpendingAntigravity())
            {
                _model.CurrentHeight += _model.HeightChangeSpeed * Time.fixedDeltaTime * (_view.IsMovingUp() ? 1 : -1);
                _controllerAntigravity.SpendAntigravityTick();
            }

            _view.TickPosition(_controllerEngine.GetEngineStatus() == EShipEngineStatus.Working,
                _controllerAntigravity.HasAntigravity()); 
        }


        public int GetSpeedMultiplier() => _model.CurrentSpeedMultiplier;

        //TODO: Check for zero
        public void Tick() =>
            _controllerAntigravity.Tick(_model.CurrentHeight / (_model.HeightMax - _model.HeightMin));
    }
}