﻿namespace GameSystems.Ship.ShipCore
{
    public struct ShipModel
    {
        public int SpeedMultiplierMax;
        public int SpeedMultiplierMin;
        public float SpeedStep;

        public float HeightMin;
        public float HeightMax;
        public float HeightChangeSpeed;
        
        public float CurrentSpeed;
        public int CurrentSpeedMultiplier;
        public float CurrentHeight;
    }
}