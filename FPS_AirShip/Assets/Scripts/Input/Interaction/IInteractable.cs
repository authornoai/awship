﻿namespace AWShip.Input.Interaction
{
    public interface IInteractable
    {
        void OnInteract_Start(bool isLeft);
        void OnInteract_End(bool isLeft);
        
        string GetLocalizedString(bool isLeft);
    }
}