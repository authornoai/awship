﻿using UnityEngine.InputSystem;
using Zenject;
using Keyboard = UnityEngine.InputSystem.Keyboard;

namespace AWShip.Input
{
    public class MasterInput : ILateTickable
    {
        private GameControlInput _input;
        
        public GameControlInput GetInputAsset() => _input;
        public Gamepad CurrentDeviceGamepad => Gamepad.current;
        public Mouse CurrentDeviceMouse => Mouse.current;
        public Keyboard CurrentDeviceKeyboard => Keyboard.current;

        private int _mouseVal;

        public MasterInput()
        {
            _input = new GameControlInput();
            _input.Enable();
        }

        private bool MouseIsMove() => _mouseVal != (int) Mouse.current.position.ReadValue().magnitude;
        public void LateTick()
        {
            _mouseVal = (int) Mouse.current.position.ReadValue().magnitude;
        }
    }
}