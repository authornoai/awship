﻿using AWShip.Character;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

namespace AWShip.Input
{
    public class PlayerInputLinker : IInitializable
    {
        private readonly MasterInput _input;
        private readonly GameControlInput _inputAsset;

        private readonly Walkable _playerWalk;
        private readonly Lookable _playerLook;
        private readonly PlayerInteractor _playerInter;
        
        public PlayerInputLinker(MasterInput input, Walkable playerWalk, Lookable playerLook,
            PlayerInteractor playerInter)
        {
            _input = input;
            _inputAsset = _input.GetInputAsset();

            _playerWalk = playerWalk;
            _playerLook = playerLook;
            _playerInter = playerInter;
        }
        
        public void Initialize()
        {
            _inputAsset.Player.Pass_Move.performed += Input_OnMove;
            _inputAsset.Player.Pass_Look.performed += Input_OnLook;
            _inputAsset.Player.Action_Jump.performed += Input_OnJump;
            _inputAsset.Player.Action_Run.performed += Input_OnRun;
            
            _inputAsset.Player.Action_InterLeft.performed += Input_OnInterLeft;
            _inputAsset.Player.Action_InterRight.performed += Input_OnInterRight;
        }

        private void Input_OnInterRight(InputAction.CallbackContext obj)
        {
            var isPressed = obj.ReadValueAsButton();
            
            if(isPressed) _playerInter.DoInterRight();
            else _playerInter.FinishInterRight();
        }

        private void Input_OnInterLeft(InputAction.CallbackContext obj)
        {
            var isPressed = obj.ReadValueAsButton();
            
            if(isPressed) _playerInter.DoInterLeft();
            else _playerInter.FinishInterLeft();
        }

        private void Input_OnRun(InputAction.CallbackContext obj) => _playerWalk.SetRunning(obj.ReadValueAsButton());
        private void Input_OnJump(InputAction.CallbackContext obj) => _playerWalk.DoJump();

        private void Input_OnLook(InputAction.CallbackContext obj)
        {
            _playerLook.SetRotationDir(obj.ReadValue<Vector2>());
        }
        private void Input_OnMove(InputAction.CallbackContext obj)
        {
            var input = obj.ReadValue<Vector2>();
            var translatedInput = new Vector3(input.x, 0, input.y);
            _playerWalk.SetMoveDir(translatedInput);
        }
    }
}