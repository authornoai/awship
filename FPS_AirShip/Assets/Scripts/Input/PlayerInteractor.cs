﻿using AWShip.Input.Interaction;
using UI;
using UnityEngine;
using Zenject;

namespace AWShip.Input
{
    public sealed class PlayerInteractor : ITickable, IInitializable
    {
        private readonly Camera _camera;
        private readonly PlayerInteractorView _view;

        private bool _isActive;
        
        private IInteractableLeft _targetLeft;
        private IInteractableRight _targetRight;
        
        private IInteractableLeft _targetLeftInInter;
        private IInteractableRight _targetRightInInter;
        
        private readonly LayerMask _mask = LayerMask.NameToLayer("Interactable");

        public PlayerInteractor(Camera camera, PlayerInteractorView view)
        {
            _camera = camera;
            _view = view;
        }
        
        public void Initialize()
        {
            _isActive = true;
            _view.NoViewForced();
        }

        public void SetActiveInteractor(bool toSet) => _isActive = toSet;
        
        public void DoInterRight() => DoInter(false);
        public void DoInterLeft() => DoInter();

        public void FinishInterRight() => FinishInter(false);
        public void FinishInterLeft() => FinishInter();
        
        public void Tick()
        {
            Ray ray = _camera.ScreenPointToRay(
                new Vector3(_camera.pixelWidth / 2.0f, _camera.pixelHeight / 2.0f));
            
            if (!Physics.Raycast(ray, out RaycastHit hit, 5.0f, 1 << _mask.value))
            {
                _targetLeft = null;
                _targetRight = null;
                _view.NoView();
                return;
            }

            Transform target = hit.transform;
            _targetLeft = target.GetComponent<IInteractableLeft>();
            _targetRight = target.GetComponent<IInteractableRight>();
            
            if(_targetLeftInInter != _targetLeft) FinishInterLeft();
            if(_targetRightInInter != _targetRight) FinishInterRight();
            
            if (!_isActive)
            {
                _view.NoView();
                return;
            }
            
            _view.SetView(_targetLeft, _targetRight);
        }

        private void DoInter(bool isLeft = true)
        {
            if(!_isActive) return;

            if (isLeft && _targetLeft != null)
            {
                _targetLeft.OnInteract_Start(true);
                _targetLeftInInter = _targetLeft;
            }
            else if (_targetRight != null)
            {
                _targetRight.OnInteract_Start(false);
                _targetRightInInter = _targetRight;
            }
        }

        private void FinishInter(bool isLeft = true)
        {
            if(!_isActive) return;

            if (isLeft)
            {
                _targetLeftInInter?.OnInteract_End(true);
                _targetLeftInInter = null;
            }
            else
            {
                _targetRightInInter?.OnInteract_End(true);
                _targetRightInInter = null;
            }
        }


        
    }
}