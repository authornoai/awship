﻿namespace BL
{
    public interface IIsPlayed
    {
        public bool IsPlayed { get; set; }
    }
}