﻿
namespace BL
{
    public struct CooldownLogic
    {
        public bool CanDo;
        public bool IsDoing;

        public float CooldownMax;
        public float CooldownCurrent;

        public CooldownLogic(float maxCooldownTime = 0, float currentCooldownTime = 0)
        {
            CanDo = false;
            IsDoing = false;
            CooldownMax = maxCooldownTime;
            CooldownCurrent = currentCooldownTime;
        }

        public void ResetCooldown() => CooldownCurrent = CooldownMax;
        public void CooldownTick(float time) => CooldownCurrent -= time;
    }
}