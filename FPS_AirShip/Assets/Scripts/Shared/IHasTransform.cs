﻿using UnityEngine;

namespace BL
{
    public interface IHasTransform
    {
        Transform transform { get; }
    }
}