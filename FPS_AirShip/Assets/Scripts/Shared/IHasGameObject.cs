﻿using UnityEngine;

namespace BL
{
    public interface IHasGameObject
    {
        GameObject gameObject { get; }
    }
}