﻿using UnityEngine;

namespace BL
{
    public class IntIndex : MonoBehaviour
    {
        [SerializeField] private int _index;
        public int Index => _index;
    }
}