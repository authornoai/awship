﻿using UnityEngine;

namespace BL
{
    public struct FrameCooldown
    {
        private float _startTime;
        public void Capture(float time)
        {
            _startTime = time;
        }
        
        public bool IsInCooldown(float cooldownTime, float currentTime)
        {
            return currentTime - _startTime < cooldownTime;
        }

        public bool IsInCooldown(float cooldownTime)
        {
            return Time.time - _startTime < cooldownTime;
        }
    }
}