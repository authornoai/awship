﻿using System;
using System.Collections.Generic;
using BL.KoalaLogic.LogicBlock;
using Runtime;
using UnityEngine;

namespace BL.KoalaLogic.Sequence
{
    public class LogicSwitcher : LogicTargetAbstract
    {
        [SerializeField] private List<LogicSequence> _sequences;
        [SerializeField] private LogicSequence _sequenceOnAuto;
        private LogicSequence _currentSeq;

        public override bool CanLaunch(GameObject target)
        {
            _currentSeq = GetSequence(target);
            return _currentSeq && _currentSeq.CanLaunch(target);
        }

        public override void Launch(GameObject user, GameObject source, bool isAuto, Action onEnd = null)
        {
            _currentSeq = !isAuto ? GetSequence(user) : _sequenceOnAuto;
            _currentSeq.Launch(user, source, isAuto, onEnd);
        }

        public override void Interrupt()
        {
            if (!_currentSeq) return;
            _currentSeq.Interrupt();
        }

        public override float GetOverallTime(GameObject target)
        {
            _currentSeq = GetSequence(target);
            return _currentSeq.GetOverallTime(target);
        }

        public override bool IsInUse() => _currentSeq && _currentSeq.IsInUse();

        private LogicSequence GetSequence(GameObject user)
        {
            for (var index = 0; index < _sequences.Count; index++)
            {
                LogicSequence pair = _sequences[index];
                if (!pair.CanLaunch(user)) continue;
                return pair;
            }

            return null;
        }
    }
}