﻿using BL.KoalaLogic.LogicBlock;

namespace Runtime.LogicBlock.Sequence
{
    public interface ILogicSequence : ILogicTarget
    {
        void Step();
    }
}