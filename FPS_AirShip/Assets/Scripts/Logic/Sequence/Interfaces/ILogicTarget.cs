﻿using System;
using Runtime;
using UnityEngine;

namespace BL.KoalaLogic.LogicBlock
{
    public interface ILogicTarget
    {
        bool CanLaunch(GameObject target);
        void Launch(GameObject user, GameObject source, bool isAuto, Action onEnd = null);
        void Interrupt();
        float GetOverallTime(GameObject target);
        bool IsInUse();
    }
}