﻿using System;
using BL;
using BL.Services.LogicSequences;
using BLLogic.Nodes;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Runtime.LogicBlock.Sequence
{
    public class LogicSubSequence : ILogicSequence
    {
        private ILogicSequenceService _service;
        private LogicNodeActionOne[] _actions;
        private float[] _delayOnUse;
        private float[] _delayAfterUse;
        private float _timeOverallUsing;

        private int _currentInUse;
        private bool _isAfterDelay;
        private bool _isInUse;
        private FrameCooldown _currentDelay;
        private float _currentDelayTarget;

        private Action _onEnd;

        public LogicSubSequence(ILogicSequenceService service, LogicNodeActionOne[] actions, bool isCopy = false)
        {
            _service = service;
            if (isCopy)
            {
                _actions = new LogicNodeActionOne[actions.Length];
                for (int i = 0; i < _actions.Length; i++)
                {
                    _actions[i] = Object.Instantiate(actions[i]);
                }
            }
            else
            {
                _actions = actions;
            }
        }

        public void Launch(GameObject user, GameObject source, bool isAuto, Action onEnd = null)
        {
            Interrupt();

            InitDelayAndActionValues();

            _onEnd = onEnd;
            _currentDelay.Capture(Time.time);
            _currentDelayTarget = 0;
            _isAfterDelay = false;
            _currentInUse = 0;

            _isInUse = true;
            _service.HandleSequence(this);
        }

        public void Step()
        {
            if (_currentInUse == _actions.Length)
            {
                _isInUse = false;
                _onEnd?.Invoke();
                return;
            }

            if (_currentDelay.IsInCooldown(_currentDelayTarget, Time.time)) return;

            if (!_isAfterDelay)
            {
                _actions[_currentInUse].Use_Start();
                _isAfterDelay = true;
                _currentDelay.Capture(Time.time);
                _currentDelayTarget = _delayOnUse[_currentInUse];
            }
            else
            {
                _actions[_currentInUse].Use_Stop();
                _isAfterDelay = false;
                _currentDelay.Capture(Time.time);
                _currentDelayTarget = _delayAfterUse[_currentInUse];

                ++_currentInUse;
            }
        }

        public void Interrupt()
        {
            if (!_isInUse) return;
            _isInUse = false;

            for (int i = _currentInUse; i >= 0; i--)
                _actions[i].Use_Interrupt();

            _currentInUse = 0;
        }

        public float GetOverallTime(GameObject targ) => _timeOverallUsing;
        public bool IsInUse() => _isInUse;
        public bool CanLaunch(GameObject target) => true;

        private void InitDelayAndActionValues()
        {
            if (_delayOnUse != null) return;
            _delayOnUse = new float[_actions.Length];
            _delayAfterUse = new float[_actions.Length];

            for (var i = 0; i < _actions.Length; i++)
            {
                LogicNodeActionOne action = _actions[i];
                action.FillValues();
                _timeOverallUsing += action.TimeOnUse + action.TimeAfterUse;
                _delayOnUse[i] = action.TimeOnUse;
                _delayAfterUse[i] = action.TimeAfterUse;
            }
        }
    }
}