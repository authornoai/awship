﻿using System;
using System.Collections.Generic;
using BL.KoalaLogic.Check;
using BL.LogicBlock.NodeBased.Graph;
using BL.Services.LogicSequences;
using BLLogic.Context;
using BLLogic.Nodes;
using Runtime.LogicBlock.Sequence;
using TypeReferences;
using UnityEditor;
using UnityEngine;
using Zenject;

namespace BL.KoalaLogic.LogicBlock
{
    public class LogicSequence : 
        LogicTargetAbstract, 
        IInitializable, 
        ILogicSequence
    {
        [Inject] private ILogicSequenceService _service;

        [SerializeField] private bool _playOnce = false;
        [SerializeField] private LogicGraph _graph;

        public LogicGraph Graph => _graph;
        private ILogicCheck[] _checks;
        private LogicNodeActionOne[] _actions;
        
        private float[] _delayOnUse;
        private float[] _delayAfterUse;
        private GameObject _currentUser;
        private GameObject _currentSource;
        private int _currentInUse;
        private bool _isAfterDelay;
        private bool _isInUse;
        private FrameCooldown _currentDelay;
        private float _currentDelayTarget;
        private Action _onEnd;
        private float _timeOverallUsing;
        
        public bool IsPlayed { get; set; }

        public override bool IsInUse() => _isInUse;
        
        [SerializeField, HideInInspector] private GameObject[] _translated;
        [SerializeField, HideInInspector] private float[] _translatedFloats;
        [SerializeField, HideInInspector] private int[] _translatedInts;
        [SerializeField, HideInInspector] private bool[] _translatedBools;
        [SerializeField, HideInInspector] private Vector3[] _translatedVecs3;
        
#if UNITY_EDITOR

        [SerializeField, HideInInspector] public string[] _eTranslatedKeys;
        [SerializeField, HideInInspector] public TypeReference[] _eTranslatedRefs;
        
        [SerializeField, HideInInspector] public string[] _eIntKeys;
        [SerializeField, HideInInspector] public string[] _eFloatKeys;
        [SerializeField, HideInInspector] public string[] _eBoolKeys;
        [SerializeField, HideInInspector] public string[] _eVec3Keys;

        private void FillTranslatedObjects()
        {
            var nodes = _graph.TranslatedNodes;
            if (nodes == null || nodes.Length == 0)
            {
                _translated = null;
                return;
            }
            List<TypeReference> refs = new List<TypeReference>();
            List<string> names = new List<string>();
            foreach (LNGetTranslated node in nodes)
            {
                refs.Add(node.TargetType);
                names.Add(node.Key);
            }
            _eTranslatedRefs = refs.ToArray();
            _eTranslatedKeys = names.ToArray();
        }
        private void FillTranslatedValues<T,TU>(T[] nodes, out string[] keys, ref TU[] toClearIfNo) 
            where T : LNGetTranslatedValue<TU>
            where TU : struct
        {
            if (nodes == null)
            {
                toClearIfNo = null;
                keys = null;
                return;
            }
            
            List<string> names = new List<string>();
            foreach (T node in nodes)
            {
                names.Add(node.Key);
            }
            keys = names.ToArray();
        }
        
        private void OnValidate()
        {
            if (_graph)
            {
                if(EditorApplication.isPlaying) return;
                FillTranslatedObjects();
                FillTranslatedValues(_graph.TranslatedBoolNodes, out _eBoolKeys, ref _translatedBools);
                FillTranslatedValues(_graph.TranslatedIntNodes, out _eIntKeys, ref _translatedInts);
                FillTranslatedValues(_graph.TranslatedVec3NodesNodes, out _eVec3Keys, ref _translatedVecs3);
                FillTranslatedValues(_graph.TranslatedFloatNodes, out _eFloatKeys, ref _translatedFloats);
            }
            else
            {
                UnityEditor.EditorApplication.delayCall += () =>
                {
                    var check = GetComponent<LogicGraphScene>();
                    if (!check)
                    {
#if UNITY_EDITOR
                        Debug.LogError($"LogicSequence: {gameObject.name} there is no" +
                                       $"neither LogicGraph link, nor LogicGraphScene component");
#endif
                        return;
                    }

                    _graph = check.graph;
                };
            }
        }
#endif
        
        public void Initialize()
        {
            _checks = GetComponentsInChildren<ILogicCheck>();
        }

        public override bool CanLaunch(GameObject target)
        {
            if (!_graph || _playOnce && IsPlayed) return false; //TEMP CODE

            if (_checks == null) return true;
            for (var i = 0; i < _checks.Length; i++)
            {
                if (!_checks[i].IsPassed(target)) return false;
            }

            return true;
        }
        public override void Launch(GameObject user, GameObject source, bool isAuto, Action onEnd = null)
        {
            if (!_graph) return; //TEMP CODE

            if (!isAuto)
            {
                if (_playOnce && IsPlayed) return;
                if (CanLaunch(user) == false) return;
            }

            Interrupt();

            _currentSource = source;
            _currentUser = user;

            SetupGraphData(user, source);

            InitDelayAndActionValues();

            IsPlayed = true;
            _onEnd = onEnd;
            _currentDelay.Capture(Time.time);
            _currentDelayTarget = 0;
            _isAfterDelay = false;
            _currentInUse = 0;

            _isInUse = true;
            _service.HandleSequence(this);
        }

        private void SetupGraphData(GameObject user, GameObject source)
        {
            _graph.SetUser(user);
            _graph.SetSource(source);
            _graph.SetTranslated(_translated,
                _translatedInts, _translatedFloats, _translatedVecs3, _translatedBools);
        }
        private void InitDelayAndActionValues()
        {
            if (_delayOnUse != null)
            {
                for (var i = 0; i < _actions.Length; i++)
                {
                    LogicNodeActionOne action = _actions[i];
                    action.FillValues();
                }

                return;
            }

            _actions = _graph.Actions;
            _delayOnUse = new float[_actions.Length];
            _delayAfterUse = new float[_actions.Length];

            for (var i = 0; i < _actions.Length; i++)
            {
                LogicNodeActionOne action = _actions[i];
                action.FillValues();
                _timeOverallUsing += action.TimeOnUse + action.TimeAfterUse;
                _delayOnUse[i] = action.TimeOnUse;
                _delayAfterUse[i] = action.TimeAfterUse;
            }
        }

        public void Step()
        {
            if (_currentInUse == _actions.Length)
            {
                _currentUser = null;
                _currentSource = null;

                _isInUse = false;
                _graph.SetUser(null);
                _graph.SetSource(null);

                _onEnd?.Invoke();
                return;
            }

            if (_currentDelay.IsInCooldown(_currentDelayTarget, Time.time)) return;

            if (!_isAfterDelay)
            {
                _actions[_currentInUse].Use_Start();
                _isAfterDelay = true;
                _currentDelay.Capture(Time.time);
                _currentDelayTarget = _delayOnUse[_currentInUse];
            }
            else
            {
                _actions[_currentInUse].Use_Stop();
                _isAfterDelay = false;
                _currentDelay.Capture(Time.time);
                _currentDelayTarget = _delayAfterUse[_currentInUse];

                ++_currentInUse;
            }
        }

        public override void Interrupt()
        {
            if (!_isInUse) return;
            _isInUse = false;

            for (int i = _currentInUse; i >= 0; i--)
                _actions[i].Use_Interrupt();

            _currentInUse = 0;
            _graph.SetUser(null);
        }

        public override float GetOverallTime(GameObject targ) => _timeOverallUsing;
    }
}