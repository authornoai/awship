﻿using System;
using Runtime;
using UnityEngine;

namespace BL.KoalaLogic.LogicBlock
{
    public abstract class LogicTargetAbstract : MonoBehaviour, ILogicTarget
    {
        public abstract bool CanLaunch(GameObject target);
        public abstract void Launch(GameObject user, GameObject source, bool isAuto, Action onEnd = null);
        public abstract void Interrupt();
        public abstract float GetOverallTime(GameObject target);
        public abstract bool IsInUse();
    }
}