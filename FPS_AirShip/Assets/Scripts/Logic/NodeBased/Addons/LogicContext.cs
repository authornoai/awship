﻿using BL.LogicBlock.NodeBased.Addons.ContextSO;
using Runtime.LogicBlock.NodeBased.Service;
using UnityEngine;
using Zenject;

namespace BL.LogicBlock.NodeBased.Addons
{
    public class LogicContext : MonoBehaviour, ILogicContext
    {
        [SerializeField] private NodeContextSO _node;
        [Inject] private ISceneNodeContextService _context;

        public void Initialize()
        {
#if UNITY_EDITOR
            if (_node == null)
            {
                Debug.LogError($"Reference SO context not created:{name}");
                return;
            }

#endif
            _context.AddContext(_node.Hash, this);
        }
    }
}