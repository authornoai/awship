﻿using UnityEngine;

namespace BL.LogicBlock.NodeBased.Addons.ContextSO
{
    [CreateAssetMenu(fileName = "NodeContext_", menuName = "SibKoala/Logic/Create Context", order = 0)]
    public class NodeContextSO : ScriptableObject
    {
        [SerializeField] private string _name;
        [SerializeField] private int _hash;
        public int Hash => _hash;

        private void OnValidate()
        {
            _hash = _name.GetHashCode();
        }
        
        #if UNITY_EDITOR
        public void SetName(string name)
        {
            _name = name;
            _hash = _name.GetHashCode();
        }
        #endif
    }
}