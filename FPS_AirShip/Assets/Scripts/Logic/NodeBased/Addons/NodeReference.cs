﻿using System;
using UnityEngine;
using XNode;

namespace BL.LogicBlock.NodeBased.Addons
{
    [Serializable]
    public class NodeReference
    {
        [SerializeField] private Node _node;
        public Node GetNode() => _node;
        public void SetNode(Node n) => _node = n;
    }
}