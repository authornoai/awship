﻿using System;
using System.Collections.Generic;
using System.Linq;
using BL.LogicBlock.NodeBased.Addons;
using BL.LogicBlock.NodeBased.Addons.ContextSO;
using UnityEngine;
using Zenject;

namespace Runtime.LogicBlock.NodeBased.Service
{
    public class SceneNodeContextService : ISceneNodeContextService, IInitializable
    {
        private Dictionary<int, LogicContext> _context;
        private Dictionary<int, List<LogicContext>> _duplicatedContext;


        public void Initialize()
        {
            _context = new Dictionary<int, LogicContext>();
            _duplicatedContext = new Dictionary<int, List<LogicContext>>();
        }

        public void AddContext(int hash, LogicContext logic)
        {
            if (_context.ContainsKey(hash))
            {
                if (!_duplicatedContext.ContainsKey(hash))
                {
                    _duplicatedContext.Add(hash, new List<LogicContext>());
                }

                _duplicatedContext[hash].Add(logic);
#if UNITY_EDITOR
                Debug.Log(
                    $"LogicContext: There is a new reference of {logic} - {logic.gameObject} with index {_duplicatedContext[hash].Count + 1}");
#endif
            }
            else
            {
                _context.Add(hash, logic);
            }
        }

        public LogicContext GetContext(NodeContextSO nodeSO, int objWithContext)
        {
#if UNITY_EDITOR
            if (!_context.ContainsKey(nodeSO.Hash))
                Debug.LogError(
                    $"Trying to get non existing in scene context {nodeSO.name}");
#endif

            return objWithContext == 0 ? _context[nodeSO.Hash] : _duplicatedContext[nodeSO.Hash][objWithContext - 1];
        }
    }
}