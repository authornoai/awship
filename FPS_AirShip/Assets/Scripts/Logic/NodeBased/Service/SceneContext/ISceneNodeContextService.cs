﻿using BL.LogicBlock.NodeBased.Addons;
using BL.LogicBlock.NodeBased.Addons.ContextSO;

namespace Runtime.LogicBlock.NodeBased.Service
{
    public interface ISceneNodeContextService
    {
        void AddContext(int hash, LogicContext logic);
        LogicContext GetContext(NodeContextSO nodeSO, int objWithContext);
    }
}