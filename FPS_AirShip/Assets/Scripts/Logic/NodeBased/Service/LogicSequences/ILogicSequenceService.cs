﻿using Runtime.LogicBlock.Sequence;

namespace BL.Services.LogicSequences
{
    public interface ILogicSequenceService
    {
        void HandleSequence(ILogicSequence sequence);
    }
}