﻿using System.Collections.Generic;
using Runtime.LogicBlock.Sequence;
using Zenject;

namespace BL.Services.LogicSequences
{
    public class LogicSequenceService : IInitializable, ITickable, ILogicSequenceService
    {
        private List<ILogicSequence> _activePlays;
        private List<int> _activePlaysToDestroy;
        private bool _isInPause;
        
        public void Initialize()
        {
            _activePlays = new List<ILogicSequence>(4);
            _activePlaysToDestroy = new List<int>(4);
            _isInPause = false;
        }
        
        public void HandleSequence(ILogicSequence sequence)
        {
            _activePlays.Add(sequence);
        }

        public void Tick()
        {
            if(_activePlays.Count <= 0 || _isInPause) return;

            _activePlaysToDestroy.Clear();

            for (int i = 0; i < _activePlays.Count; i++)
            {
                ILogicSequence currentPlay = _activePlays[i];

                if (!currentPlay.IsInUse()) _activePlaysToDestroy.Add(i);
                else currentPlay.Step();
       
            }

            if(_activePlaysToDestroy.Count <= 0) return;
            _activePlays.RemoveAt(_activePlaysToDestroy[0]);
            
        }
    }
}