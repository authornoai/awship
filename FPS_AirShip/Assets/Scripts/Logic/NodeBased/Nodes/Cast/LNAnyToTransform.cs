﻿using BL.LogicBlock.NodeBased.Addons;
using BLLogic.Nodes;
using UnityEngine;
using XNode;

namespace BLLogic.Cast
{
    [NodeTint(255,0,0)]
    public class LNAnyToTransform : LogicNodeCast
    {
        [Input(ShowBackingValue.Never, connectionType = ConnectionType.Override), SerializeField] private Object _target;
        [Output, SerializeField] private Transform _transform;

        public override object GetValue(NodePort port)
        {
            _target = GetInputValue<Object>(nameof(_target));
            switch (_target)
            {
                case null:
                    return null;
                case Component targetComponent:
                    _transform = targetComponent.transform;
                    return _transform ? _transform : null;
                case GameObject targetGO:
                    _transform = targetGO ? targetGO.transform : null;
                    return _transform ? _transform : null;
            }

            if (!(_target is Transform targetTrans)) return null;
            
            _transform = targetTrans.transform;
            return _transform ? _transform : null;
        }

        private const string _cConstName = "Transform";
        protected override string GetCastName() => _cConstName;
    }
}