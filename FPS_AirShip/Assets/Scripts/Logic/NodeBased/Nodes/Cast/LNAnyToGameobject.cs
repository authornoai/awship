﻿using BLLogic.Nodes;
using UnityEngine;
using XNode;

namespace BLLogic.Cast
{
    [NodeTint(255,0,0)]
    public class LNAnyToGameobject : LogicNodeCast
    {
        [Input(ShowBackingValue.Never, connectionType = ConnectionType.Override), SerializeField] private Object _target;
        [Output, SerializeField] private GameObject _gameObj;

        public override object GetValue(NodePort port)
        {
            _target = GetInputValue<Object>(nameof(_target));
            switch (_target)
            {
                case null:
                    return null;
                case Component targetComponent:
                    _gameObj = targetComponent.gameObject;
                    return _gameObj ? _gameObj : null;
                case GameObject targetGO:
                    _gameObj = targetGO.gameObject;
                    return _gameObj ? _gameObj : null;
            }

            if (!(_target is Transform targetTrans)) return null;
            
            _gameObj = targetTrans.gameObject;
            return _gameObj ? _gameObj : null;
        }
        
        private const string _cConstName = "Game Object";
        protected override string GetCastName() => _cConstName;
    }
}