﻿using BLLogic.Nodes;
using UnityEngine;
using XNode;

namespace BLLogic.Cast
{
    public abstract class LNAnyToT<T> : LogicNodeCast where T : Object
    {
        [Input(ShowBackingValue.Never, connectionType = ConnectionType.Override), SerializeField] private Object _target;
        [Input(ShowBackingValue.Never, connectionType = ConnectionType.Override), SerializeField] private Object _tagetFallBack;
        [Output, SerializeField] private T _result;

        public override object GetValue(NodePort port)
        {
            _target = GetInputValue<Object>(nameof(_target));
            _tagetFallBack = GetInputValue<Object>(nameof(_tagetFallBack));
            _result = TryCast(_target);
            if (_result == null) _result = TryCast(_tagetFallBack);
            return _result;
        }
        
        protected override string GetCastName() => typeof(T).Name;

        private static T TryCast(Object target)
        {
            if (target == null) return null;
            return target switch
            {
                Transform targetTrans => targetTrans.GetComponent<T>(),
                Component targetComponent => targetComponent.GetComponent<T>(),
                GameObject targetGO => targetGO.GetComponent<T>(),
                _ => null
            };
        }
    }
}