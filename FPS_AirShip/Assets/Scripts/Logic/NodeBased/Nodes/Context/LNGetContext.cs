﻿using BL.LogicBlock.NodeBased.Addons;
using BL.LogicBlock.NodeBased.Addons.ContextSO;
using BLLogic.Nodes;
using Runtime.LogicBlock.NodeBased.Service;
using UnityEngine;
using XNode;
using Zenject;

namespace BLLogic.Context
{
    public class LNGetContext : LogicNodeContext
    {
        [Output, SerializeField] private LogicContext _context;
        [SerializeField] private NodeContextSO _contextI;
        [SerializeField] private int _instanceNumber;
        [Inject] private ISceneNodeContextService _contextService;

        public override object GetValue(NodePort port)
        {
            if (!_contextI || _contextService == null) return null;
            _context = _contextService.GetContext(_contextI, _instanceNumber);
            return _context && port.fieldName == nameof(_context) ? _context : null;
        }

#if UNITY_EDITOR

        private const string C_NODE_CONTEXT_PREFIX = "NodeContext_";
        private const string C_NODE_CONTEXT_DEFAULT_NAME = "Context";
        
        private void OnValidate()
        {
            if (_contextI)
            {
                var withoutPrefix = _contextI.name.Replace(C_NODE_CONTEXT_PREFIX, string.Empty);
                name = withoutPrefix;
            }
            else
            {
                name = C_NODE_CONTEXT_DEFAULT_NAME;
            }
        }
#endif
    }
}