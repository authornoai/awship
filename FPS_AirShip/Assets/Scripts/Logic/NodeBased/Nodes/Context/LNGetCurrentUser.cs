﻿using BL.LogicBlock.NodeBased.Graph;
using BLLogic.Nodes;
using Runtime;
using UnityEngine;
using XNode;

namespace BLLogic.Context
{
    public class LNGetCurrentUser: LogicNodeContext
    {
        [Output, SerializeField] private GameObject _user;

        public override object GetValue(NodePort port)
        {
            var graphL = graph as LogicGraph;
            return graphL && graphL.User != null && port.fieldName == nameof(_user) ? graphL.User : null;
        }
    }
}