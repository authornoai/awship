﻿using BL.LogicBlock.NodeBased.Graph;
using BLLogic.Nodes;
using TypeReferences;
using UnityEngine;
using XNode;

namespace BLLogic.Context
{
    public class LNGetTranslated : LogicNodeContext, IGetTranslated
    {
        public string Key;
        public TypeReference TargetType;
        [Output, SerializeField] private GameObject _translated;

        public override object GetValue(NodePort port)
        {
            return  port.fieldName == nameof(_translated) ?
                (graph as LogicGraph)?.Translated[Key] : null;
        }
        
        public string GetKey() => Key;

#if UNITY_EDITOR
        private void OnValidate()
        {
            (graph as LogicGraph)?.UpdateTranslatedTargets();
        }

        private void OnDestroy()
        {
            (graph as LogicGraph)?.UpdateTranslatedTargets();
        }
#endif
    }
}