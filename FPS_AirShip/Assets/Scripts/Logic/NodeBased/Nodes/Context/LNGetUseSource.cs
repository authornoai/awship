﻿using BL.LogicBlock.NodeBased.Graph;
using BLLogic.Nodes;
using UnityEngine;
using XNode;

namespace BLLogic.Context
{
    public class LNGetUseSource : LogicNodeContext
    {
        [Output, SerializeField] private GameObject _source;

        public override object GetValue(NodePort port)
        {
            var graphL = graph as LogicGraph;
            return graphL && graphL.UseSource && port.fieldName == nameof(_source) ? graphL.UseSource: null;
        }
    }
}