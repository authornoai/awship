﻿using BL.LogicBlock.NodeBased.Graph;
using BLLogic.Nodes;
using UnityEngine;
using XNode;

namespace BLLogic.Context
{
    public abstract class LNGetTranslatedValue<T> : LogicNodeContext, IGetTranslated
        where T : struct
    {
        public string Key;
        [Output, SerializeField] private T _translated;

        public override object GetValue(NodePort port)
        {
            return  port.fieldName == nameof(_translated) ?
                (graph as LogicGraph)?.TranslatedAll[typeof(T)][Key] : null;
        }

        public string GetKey() => Key;

#if UNITY_EDITOR
        private void OnValidate()
        {
            (graph as LogicGraph)?.UpdateTranslatedTargets();
        }

        private void OnDestroy()
        {
            (graph as LogicGraph)?.UpdateTranslatedTargets();
        }
#endif
    }
}