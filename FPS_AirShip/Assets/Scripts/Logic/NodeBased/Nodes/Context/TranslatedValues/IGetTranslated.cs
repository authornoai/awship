﻿namespace BLLogic.Context
{
    public interface IGetTranslated
    {
        string GetKey();
    }
}