﻿using BLLogic.Context;

namespace BLLogic.Context
{
    public class LNGetBool : LNGetTranslatedValue<bool> { }
}