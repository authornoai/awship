﻿using System;

namespace BLLogic.Nodes
{
    
    [NodeTintAttribute(0.5f, 0, 0.5f), NodeWidth(128)]
    public abstract class LogicNodeCast : LogicNodeAbstract 
    {
        private void OnValidate()
        {
            name = GetCastName();
        }

        protected abstract string GetCastName();
    }
}