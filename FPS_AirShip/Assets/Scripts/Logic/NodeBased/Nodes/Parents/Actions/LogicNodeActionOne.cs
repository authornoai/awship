﻿using BL.LogicBlock.NodeBased.Addons;
using UnityEngine;

#if UNITY_EDITOR
using System;
using UnityEditor;
using XNodeEditor;
using System.Linq;
using System.Reflection;
using XNode;
using BLLogic.Cast;
#endif

namespace BLLogic.Nodes
{
    [NodeTint(0, 0.5f, 0.5f)]
    public abstract class LogicNodeActionOne : LogicNodeAbstract
    {
        [Output(connectionType = ConnectionType.Override), SerializeField]
        private NodeReference _next;

        public abstract float TimeOnUse { get; }
        public abstract float TimeAfterUse { get; }

        public abstract void Use_Start();
        public abstract void Use_Stop();
        public abstract void Use_Interrupt();

        public abstract void FillValues();


        protected void FillValue<T>(ref T setTarget, string nameOfSet)
        {
            T temp = GetInputValue(nameOfSet, setTarget);
            setTarget = temp;
        }


#if UNITY_EDITOR
        public override void OnCreateConnection(NodePort fromNode, NodePort toNode)
        {
            AddCastToNode(fromNode, toNode);
        }

        private void AddCastToNode(NodePort fromNode, NodePort toNode)
        {
            if ((toNode.IsInput && toNode.node.GetType().IsSubclassOf(typeof(LogicNodeCast)))
                || (toNode.node != this))
            {
                return;
            }

            Type typeFrom = fromNode.ValueType;
            Type typeTo = toNode.ValueType;
            if (typeFrom == typeTo) return;
            Debug.Log($"TypeFrom:{typeFrom} \n TypeTo{typeTo}");

            fromNode.Disconnect(toNode);

            Node node = null;
            if (typeTo == typeof(Transform))
            {
                Undo.RecordObject(graph, "Create Node");
                node = graph.AddNode(typeof(LNAnyToTransform));
            }
            else if (typeTo == typeof(GameObject))
            {
                Undo.RecordObject(graph, "Create Node");
                node = graph.AddNode(typeof(LNAnyToGameobject));
            }
            else if (typeFrom.IsSubclassOf(typeof(UnityEngine.Object)))
            {
                var castTypes =
                    Assembly.GetAssembly(typeof(LNAnyToT<>)).GetTypes().AsParallel().Where(
                        (t) => t.Namespace == typeof(LNAnyToT<>).Namespace).ToArray();
                Type resultType = castTypes.Find((t => t.BaseType != null
                                                       && t.BaseType.GenericTypeArguments.Length > 0
                                                       && t.BaseType.GenericTypeArguments[0] == typeTo));
                if (resultType == null) return;

                Undo.RecordObject(graph, "Create Node");
                node = graph.AddNode(resultType);
            }

            if (node == null) return;

            Undo.RegisterCreatedObjectUndo(node, "Create Node");
            node.position = Vector2.Lerp(fromNode.node.position, toNode.node.position, 0.5f);

            if (node.name.Trim() == "") node.name = NodeEditorUtilities.NodeDefaultName(node.GetType());
            if (!string.IsNullOrEmpty(AssetDatabase.GetAssetPath(node.graph)))
                AssetDatabase.AddObjectToAsset(node, node.graph);
            if (NodeEditorPreferences.GetSettings().autoSave) AssetDatabase.SaveAssets();
            NodeEditorWindow.RepaintAll();

            fromNode.Connect(node.Inputs.First());
            toNode.Connect(node.Outputs.First());
        }

#endif
    }
}