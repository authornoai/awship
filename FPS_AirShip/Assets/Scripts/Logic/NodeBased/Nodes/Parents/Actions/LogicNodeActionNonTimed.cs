﻿using BL.LogicBlock.NodeBased.Addons;
using UnityEngine;

namespace BLLogic.Nodes
{
    public abstract class LogicNodeActionNonTimed : LogicNodeActionOne
    {
        [Input(connectionType = ConnectionType.Override), SerializeField] private NodeReference _previous;
    }
}