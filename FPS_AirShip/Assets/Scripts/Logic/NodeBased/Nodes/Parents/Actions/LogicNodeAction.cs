﻿using BL.LogicBlock.NodeBased.Addons;
using UnityEngine;

namespace BLLogic.Nodes
{
    public abstract class LogicNodeAction : LogicNodeActionOneTimed 
    {
        [Input(connectionType = ConnectionType.Override), SerializeField] private NodeReference _previous;
    }
}