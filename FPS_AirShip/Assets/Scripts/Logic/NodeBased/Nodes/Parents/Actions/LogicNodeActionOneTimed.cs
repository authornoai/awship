﻿using UnityEngine;

namespace BLLogic.Nodes
{
    public abstract class LogicNodeActionOneTimed : LogicNodeActionOne
    {
        [SerializeField] protected float _timeOnUse;
        [SerializeField] protected float _timeAfterUse;

        public override float TimeOnUse => _timeOnUse;
        public override float TimeAfterUse => _timeAfterUse;
    }
}