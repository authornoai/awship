﻿using Zenject;

namespace BLLogic.Nodes
{
    public interface ILogicTickableNode : ILogicNode, ITickable
    {
        
    }
}