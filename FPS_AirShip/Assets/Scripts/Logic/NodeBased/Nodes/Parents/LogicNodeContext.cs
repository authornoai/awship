﻿using XNode;

namespace BLLogic.Nodes
{
    [Node.NodeTintAttribute(0.5f, 0.5f, 0)]
    public abstract class LogicNodeContext : LogicNodeAbstract
    { }
}