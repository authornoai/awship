﻿using System.Collections.Generic;
using BL.LogicBlock.NodeBased.Addons;
using BL.LogicBlock.NodeBased.Addons.ContextSO;
using BL.LogicBlock.NodeBased.Graph;
using BL.Services.LogicSequences;
using BLLogic.Nodes;
using Runtime.LogicBlock.NodeBased.Service;
using Runtime.LogicBlock.Sequence;
using UnityEngine;
using XNode;
using Zenject;

namespace BLLogic.Special
{
    public class LNFor : LogicNodeAction
    {
        [Output(connectionType = ConnectionType.Override), SerializeField]
        private NodeReference _forTarget;

        private List<LogicNodeActionOne> _nodes;
        private List<LogicSubSequence> _logic;

        [Inject] private ILogicSequenceService _logicService;
        [Inject] private ISceneNodeContextService _contextService;

        [SerializeField] private NodeContextSO[] _contexts;
        [Output, SerializeField] private LogicContext _currentContext;

        public override void Use_Start()
        {
            if (_nodes == null || _nodes.Count == 0) return;

            for (int i = 0; i < _contexts.Length; i++)
            {
                _currentContext = _contextService.GetContext(_contexts[i], 0);
                _logic[i].Launch((graph as LogicGraph)?.User, (graph as LogicGraph)?.UseSource, true);
            }
        }

        public override void Use_Stop()
        {
        }

        public override void Use_Interrupt()
        {
            if (_nodes == null || _nodes.Count == 0) return;
            for (int i = 0; i < _contexts.Length; i++)
            {
                _logic[i].Interrupt();
            }
        }

        public override void FillValues()
        {
            FillActions();

            _logic = new List<LogicSubSequence>();
            var nodesInArray = _nodes.ToArray();
            foreach (NodeContextSO unused in _contexts)
            {
                _logic.Add(new LogicSubSequence(_logicService, nodesInArray, true));
            }
        }

        private void FillActions()
        {
            _nodes = new List<LogicNodeActionOne>();
            NodePort port = GetOutputPort(nameof(_forTarget));

            while (port != null)
            {
                Node nextNode = port.Connection?.node;
                if (nextNode is LogicNodeActionOne actionToPut)
                {
                    _nodes.Add(actionToPut);
                    port = actionToPut.GetOutputPort("_next");
                }
                else
                {
                    port = null;
                }
            }
        }

        public override object GetValue(NodePort port)
        {
            return _currentContext != null ? _currentContext : null;
        }
    }
}