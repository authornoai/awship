﻿using BL.KoalaLogic.LogicBlock;
using BL.LogicBlock.NodeBased.Graph;
using BLLogic.Nodes;
using Runtime;
using UnityEngine;

namespace BLLogic.Special
{
    public class LNParallel : LogicNodeAction
    {
        [Input(connectionType = ConnectionType.Override), SerializeField]
        private LogicSequence _parallel;

        [Input(connectionType = ConnectionType.Override), SerializeField]
        private GameObject _target;

        [Input(connectionType = ConnectionType.Override), SerializeField]
        private bool _isAutoLaunch;

        public override void FillValues()
        {
            FillValue(ref _target, nameof(_target));
            FillValue(ref _parallel, nameof(_parallel));
            FillValue(ref _isAutoLaunch, nameof(_isAutoLaunch));
        }

        public override void Use_Start()
        {
            if (_parallel == null)
            {
                return;
            }

            _parallel.Launch(_target, (graph as LogicGraph)?.UseSource, _isAutoLaunch);
        }

        public override void Use_Stop()
        {
        }

        public override void Use_Interrupt()
        {
        }

        public override float TimeOnUse => 0;
        public override float TimeAfterUse => 0;
    }
}