﻿using BLLogic.Nodes;
using UnityEngine;

namespace BLLogic.Special
{
    public class LNStart : LogicNodeActionOne
    {
        [Input(connectionType = ConnectionType.Override), SerializeField] private float _startDelay;

        public override void FillValues()
        {
            FillValue(ref _startDelay, nameof(_startDelay));
        }
        
        public override float TimeOnUse => 0;
        public override float TimeAfterUse => _startDelay;

        public override void Use_Start() { }
        public override void Use_Stop() { }
        public override void Use_Interrupt() { }
    }
}