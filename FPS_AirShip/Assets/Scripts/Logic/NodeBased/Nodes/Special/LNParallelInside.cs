﻿using System.Collections.Generic;
using BL.LogicBlock.NodeBased.Addons;
using BL.LogicBlock.NodeBased.Graph;
using BL.Services.LogicSequences;
using BLLogic.Nodes;
using Runtime.LogicBlock.Sequence;
using UnityEngine;
using XNode;
using Zenject;

namespace BLLogic.Special
{
    public class LNParallelInside  : LogicNodeActionNonTimed
    {
        [Output(connectionType = ConnectionType.Override), SerializeField] private NodeReference _new;
        private LogicSubSequence _logic;

        [Inject] private ILogicSequenceService _service;
        private LogicNodeActionOne[] _actions;

        [SerializeField] private float _timeOnUse;
        [SerializeField] private float _timeAfterUse;

        public override float TimeOnUse => _timeOnUse;
        public override float TimeAfterUse => _timeAfterUse;

        public override void FillValues()
        {
            _logic = new LogicSubSequence(_service, _actions);
        }
        
        public override void Use_Start()
        {
            _logic.Launch((graph as LogicGraph)?.User, (graph as LogicGraph)?.UseSource, true);
        }
        public override void Use_Stop() { }
        public override void Use_Interrupt()
        {
            _logic.Interrupt();
        }

        public void FillActions()
        {
            List<LogicNodeActionOne> actions = new List<LogicNodeActionOne>();
            
            NodePort port = GetOutputPort(nameof(_new));
            while (port != null)
            {
                Node nextNode = port.Connection?.node;
                if (nextNode is LogicNodeActionOne actionToPut)
                {
                    actions.Add(actionToPut);
                    port = actionToPut.GetOutputPort("_next");
                }
                else
                {
                    port = null;
                }
            }

            _actions = actions.ToArray();
        }
    }
}