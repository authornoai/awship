﻿using BLLogic.Nodes;
using UnityEngine;

namespace BLLogic.Action.Phys
{
    public class LNRigidbody : LogicNodeAction
    {
        [Input(connectionType = ConnectionType.Override), SerializeField]
        private Rigidbody _target;

        [Input(connectionType = ConnectionType.Override), SerializeField]
        private bool _enable;

        private void RigidbodyStatus(bool status)
        {
            _target.isKinematic = !status;
            _target.detectCollisions = status;
        }
        public override void Use_Start()
        {
            RigidbodyStatus(_enable);
        }

        public override void Use_Stop()
        {
        }

        public override void Use_Interrupt()
        {
        }

        public override void FillValues()
        {
            FillValue(ref _target, nameof(_target));
            FillValue(ref _enable, nameof(_enable));
        }
    }
}