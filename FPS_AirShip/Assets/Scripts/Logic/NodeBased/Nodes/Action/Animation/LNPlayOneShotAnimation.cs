﻿using BLLogic.Nodes;
using UnityEngine;

namespace BLLogic.Action.Anim
{
    public class LNPlayOneShotAnimation : LogicNodeAction
    {
        [Input(connectionType = ConnectionType.Override), SerializeField] private Animation _target;
        
        public override void FillValues()
        {
            FillValue(ref _target, nameof(_target));
        }
        
        public override void Use_Start() => _target.Play();

        public override void Use_Stop() { }

        public override void Use_Interrupt() { }
    }
}