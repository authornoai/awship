﻿using BLLogic.Nodes;
using UnityEngine;

namespace BLLogic.Action.Anim
{
    public class LNAnimationLayer : LogicNodeAction
    {
        [Input(connectionType = ConnectionType.Override), SerializeField]
        private Animator _target;

        [SerializeField] private float _weight;
        [SerializeField] private string _nameLayer;

        public override void Use_Start()
        {
            int idLayer = _target.GetLayerIndex(_nameLayer);
            _target.SetLayerWeight(idLayer, _weight);
        }

        public override void Use_Stop()
        {
        }

        public override void Use_Interrupt()
        {
        }

        public override void FillValues()
        {
            FillValue(ref _target, nameof(_target));
        }
    }
}