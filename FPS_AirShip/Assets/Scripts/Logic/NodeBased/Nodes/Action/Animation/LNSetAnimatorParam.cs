﻿using System;
using BLLogic.Nodes;
using UnityEngine;
using Object = UnityEngine.Object;

namespace BLLogic.Action.Anim
{
    internal enum EAnimParamType
    {
        Int, Float, Bool, Trigger
    }
    public class LNSetAnimatorParam : LogicNodeAction
    {
        [Input(connectionType = ConnectionType.Override), SerializeField] private Animator _target;
        [Input(connectionType = ConnectionType.Override), SerializeField] private int _int;
        [Input(connectionType = ConnectionType.Override), SerializeField] private float _float;
        [Input(connectionType = ConnectionType.Override), SerializeField] private bool _bool;
        [SerializeField] private EAnimParamType _type;
        [SerializeField] private string _paramName;

        public override void FillValues()
        {
            FillValue(ref _target, nameof(_target));
            FillValue(ref _int, nameof(_int));
            FillValue(ref _float, nameof(_float));
            FillValue(ref _bool, nameof(_bool));
        }

        public override void Use_Start()
        {
            switch (_type)
            {
                case EAnimParamType.Int:
                    _target.SetInteger(_paramName, _int);
                    break;
                case EAnimParamType.Float:
                    _target.SetFloat(_paramName, _float);
                    break;
                case EAnimParamType.Bool:
                    _target.SetBool(_paramName, _bool);
                    break;
                case EAnimParamType.Trigger:
                    _target.SetTrigger(_paramName);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public override void Use_Stop() { }
        public override void Use_Interrupt() { }
    }

    
}