﻿using BLLogic.Nodes;
using UnityEngine;

namespace BLLogic.Action.Anim
{
    public class LNAnimationEnable:LogicNodeAction
    {
        [Input(connectionType = ConnectionType.Override), SerializeField] private Animator _target;
        [Input(connectionType = ConnectionType.Override), SerializeField] private bool _enable;
        public override void Use_Start()
        {
            _target.enabled = _enable;
        }

        public override void Use_Stop()
        {
          
        }

        public override void Use_Interrupt()
        {
           
        }

        public override void FillValues()
        {
            FillValue(ref _target, nameof(_target));
            FillValue(ref _enable, nameof(_enable));
        }
    }
}