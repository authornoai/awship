﻿using BLLogic.Nodes;
using UnityEngine;

namespace BLLogic.Action.TimeLN
{
    public class LNDelayWithParams : LogicNodeActionNonTimed
    {
        [Input(connectionType = ConnectionType.Override), SerializeField] private float _timeOnUse;
        [Input(connectionType = ConnectionType.Override), SerializeField] private float _timeAfterUse;
        
        public override void FillValues()
        {
            FillValue(ref _timeOnUse, nameof(_timeOnUse));
            FillValue(ref _timeAfterUse, nameof(_timeAfterUse));
        }
        
        public override float TimeOnUse { get; }
        public override float TimeAfterUse { get; }
        
        public override void Use_Start() { }
        public override void Use_Stop() { }
        public override void Use_Interrupt() { }

    }
}