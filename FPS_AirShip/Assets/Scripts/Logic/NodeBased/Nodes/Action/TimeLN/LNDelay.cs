﻿using BLLogic.Nodes;

namespace BLLogic.Action.TimeLN
{
    public class LNDelay : LogicNodeAction
    {
        public override void FillValues() { }
        
        public override void Use_Start() { }
        public override void Use_Stop() { }
        public override void Use_Interrupt() { }
    }
}