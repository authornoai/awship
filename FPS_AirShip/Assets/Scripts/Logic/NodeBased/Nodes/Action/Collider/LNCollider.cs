﻿using BLLogic.Nodes;
using UnityEngine;

namespace BLLogic.Action.Phys
{
    public class LNCollider : LogicNodeAction
    {
        [Input(connectionType = ConnectionType.Override), SerializeField] private Collider _target;
        [Input(connectionType = ConnectionType.Override), SerializeField] private bool _toSet;
        private bool _lastValue;

        public override void FillValues()
        {
            FillValue(ref _target, nameof(_target));
            FillValue(ref _toSet, nameof(_toSet));
        }

        public override void Use_Start()
        {
            _lastValue = _target.enabled;
            _target.enabled = _toSet;
        }
        public override void Use_Stop() { }
        public override void Use_Interrupt()
        {
            _target.enabled = _lastValue;
        }
    }
}