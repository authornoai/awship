﻿using BLLogic.Nodes;
using GameSystems.Ship.ShipEngine;
using Zenject;

namespace ShipLogic
{
    public class ShipStop : LogicNodeAction
    {
        [Inject] private ShipEngineSubviewTumbler _shipTumbler;
        public override void Use_Start()
        {
            _shipTumbler.OnInteract_Start(true);
            _shipTumbler.OnInteract_End(true);
        }

        public override void Use_Stop()
        {
            
        }

        public override void Use_Interrupt()
        {
            
        }

        public override void FillValues()
        {
            
        }
    }
}