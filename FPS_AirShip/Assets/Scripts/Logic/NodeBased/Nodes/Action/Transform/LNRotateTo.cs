﻿using BLLogic.Nodes;
using DG.Tweening;
using UnityEngine;

namespace BLLogic.Action.Transforms
{
    public class LNRotateTo : LogicNodeAction
    {
        [Input(connectionType = ConnectionType.Override), SerializeField] private Transform _target;
        [Input(connectionType = ConnectionType.Override), SerializeField] private Transform _lookToTarget;
        [Input(connectionType = ConnectionType.Override), SerializeField] private float _rotationTime;
        [Input(connectionType = ConnectionType.Override), SerializeField] private bool _ignoreY;

        public override void FillValues()
        {
            FillValue(ref _target, nameof(_target));
            FillValue(ref _lookToTarget, nameof(_lookToTarget));
            FillValue(ref _rotationTime, nameof(_rotationTime));
            FillValue(ref _ignoreY, nameof(_ignoreY));
        }

        public override void Use_Start()
        {
            Vector3 lookAtPos = _lookToTarget.transform.position;
            if(_ignoreY) lookAtPos.y = _target.transform.position.y;
            
            Vector3 toRotate = 
                Quaternion.LookRotation(lookAtPos - _target.transform.position)
                    .eulerAngles;
            _target.transform.DORotate(toRotate, _rotationTime);
        }
        public override void Use_Stop() { }
        public override void Use_Interrupt() { }
    }
}