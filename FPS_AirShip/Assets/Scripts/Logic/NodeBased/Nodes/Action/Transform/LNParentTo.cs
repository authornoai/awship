﻿using BLLogic.Nodes;
using UnityEngine;

namespace BLLogic.Action.Transforms
{
    public class LNParentTo : LogicNodeAction
    {
        [Input(connectionType = ConnectionType.Override), SerializeField]
        private Transform _target;

        [Input(connectionType = ConnectionType.Override), SerializeField]
        private Transform _toParent;

        [SerializeField] private bool _isUseParentPosition;
        [SerializeField] private bool _isUseParnetRotation;
        [SerializeField] private Vector3 _position;
        [SerializeField] private Vector2 _angle;

        public override void FillValues()
        {
            FillValue(ref _target, nameof(_target));
            FillValue(ref _toParent, nameof(_toParent));
        }

        public override void Use_Start()
        {
            _target.SetParent(_toParent);

            if (_isUseParentPosition)
                _target.localPosition = _position;

            if (_isUseParnetRotation)
                _target.eulerAngles = _angle;

        }


        public override void Use_Stop()
        {
        }

        public override void Use_Interrupt()
        {
        }
    }
}