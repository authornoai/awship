﻿using BLLogic.Nodes;
using DG.Tweening;
using UnityEngine;

namespace BLLogic.Action.Transforms
{
    public class LNLerpTo : LogicNodeAction
    {
        [Input(connectionType = ConnectionType.Override), SerializeField]
        private Transform _toLerp;

        [Input(connectionType = ConnectionType.Override), SerializeField]
        private Vector3 _lerpPos;

        [Input(connectionType = ConnectionType.Override), SerializeField]
        private Vector3 _lerpRot;

        [Input(connectionType = ConnectionType.Override), SerializeField]
        private bool _isLocal = true;

        [Input(connectionType = ConnectionType.Override), SerializeField]
        private bool _isRelative = true;

        [Input(connectionType = ConnectionType.Override), SerializeField]
        private bool _isPlayback;

        [Input(connectionType = ConnectionType.Override), SerializeField]
        private bool _isOnlyChangeRotate;

        [Input(connectionType = ConnectionType.Override), SerializeField]
        private float _lerpTime;

        private Transform _targTrans;
        private Vector3 _targRot;
        private Vector3 _targPos;
        private Vector3 _startRot;
        private Vector3 _startPos;

        public override void FillValues()
        {
            FillValue(ref _toLerp, nameof(_toLerp));
            FillValue(ref _lerpPos, nameof(_lerpPos));
            FillValue(ref _lerpRot, nameof(_lerpRot));
            FillValue(ref _isLocal, nameof(_isLocal));
            FillValue(ref _isRelative, nameof(_isRelative));
            FillValue(ref _isPlayback, nameof(_isPlayback));
            FillValue(ref _isOnlyChangeRotate, nameof(_isOnlyChangeRotate));
            FillValue(ref _lerpTime, nameof(_lerpTime));

            _targTrans = _toLerp.transform;
            if (_isLocal)
            {
                _startPos = _targTrans.localPosition;
                _startRot = _targTrans.localRotation.eulerAngles;
                _targPos = _isRelative ? _startPos + _lerpPos : _lerpPos;
                _targRot = _isRelative ? _startRot + _lerpRot : _lerpRot;
            }
            else
            {
                _startPos = _targTrans.position;
                _startRot = _targTrans.rotation.eulerAngles;
                _targPos = _isRelative ? _startPos + _lerpPos : _lerpPos;
                _targRot = _isRelative ? _startRot + _lerpRot : _lerpRot;
            }
        }

        public override void Use_Start()
        {
            if (_targTrans == null) return;
            
            if (_isLocal)
            {
                if (!_isOnlyChangeRotate)
                {
                    _toLerp.transform.DOLocalMove(_targPos, _lerpTime).Play();
                }

                _toLerp.transform.DOLocalRotate(_targRot, _lerpTime).Play();
            }
            else
            {
                if (!_isOnlyChangeRotate)
                {
                    _toLerp.transform.DOMove(_targPos, _lerpTime).Play();
                }

                _toLerp.transform.DORotate(_targRot, _lerpTime).Play();
            }
        }

        public override void Use_Stop()
        {
            if (!_isPlayback) return;

            if (_isLocal)
            {
                _toLerp.transform.DOLocalMove(_startPos, _lerpTime).Play();
                _toLerp.transform.DOLocalRotate(_startRot, _lerpTime).Play();
            }
            else
            {
                _toLerp.transform.DOMove(_startPos, _lerpTime).Play();
                _toLerp.transform.DORotate(_startRot, _lerpTime).Play();
            }
        }

        public override void Use_Interrupt()
        {
        }
    }
}