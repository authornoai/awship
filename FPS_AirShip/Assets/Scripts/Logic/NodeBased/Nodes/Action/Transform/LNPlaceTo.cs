﻿using BLLogic.Nodes;
using UnityEngine;

namespace BLLogic.Action.Transforms
{
    public class LNPlaceTo : LogicNodeAction
    {
        [Input(connectionType = ConnectionType.Override), SerializeField] private Transform _target;
        [Input(connectionType = ConnectionType.Override), SerializeField] private Transform _toParent;
        [Input(connectionType = ConnectionType.Override), SerializeField] private Vector3 _rotation;
        [Input(connectionType = ConnectionType.Override), SerializeField] private Vector3 _position;

        public override void FillValues()
        {
            FillValue(ref _target, nameof(_target));
            FillValue(ref _toParent, nameof(_toParent));
            FillValue(ref _rotation, nameof(_rotation));
            FillValue(ref _position, nameof(_position));
        }
        
        public override void Use_Start()
        {
            if(_toParent) _target.SetParent(_toParent);
            _target.localRotation = Quaternion.Euler(_rotation);
            _target.localPosition = _position;
        }
        public override void Use_Stop() { }
        public override void Use_Interrupt() { }
    }
}