﻿using BLLogic.Nodes;
using UnityEngine;

namespace BLLogic.Action.Transforms
{
    public class LNAdditionallyPostion:LogicNodeAction
    {
        [Input(connectionType = ConnectionType.Override), SerializeField] private Transform _target;
        [Input(connectionType = ConnectionType.Override), SerializeField] private Vector3 _pos;
        public override void Use_Start()
        {
            _target.position += _pos;
        }

        public override void Use_Stop()
        {
            
        }

        public override void Use_Interrupt()
        {
          
        }

        public override void FillValues()
        {
            FillValue(ref _target, nameof(_target));
            FillValue(ref _pos, nameof(_pos));
        }
    }
}