﻿using BLLogic.Nodes;
using DG.Tweening;
using UnityEngine;

namespace BLLogic.Action.Transforms
{
    public class LNScaleTo:LogicNodeAction
    {
        [Input(connectionType = ConnectionType.Override), SerializeField] private Transform _toScale;
        [Input(connectionType = ConnectionType.Override), SerializeField] private Vector3 _targetScale;
        [Input(connectionType = ConnectionType.Override), SerializeField] private float _duration;

        public override void FillValues()
        {
            FillValue(ref _toScale, nameof(_toScale));
            FillValue(ref _targetScale, nameof(_targetScale));
            FillValue(ref _duration, nameof(_duration));
        }
        
        public override void Use_Start() => _toScale.DOScale(_targetScale, _duration);
        public override void Use_Stop() { }
        public override void Use_Interrupt() { }
    }
}