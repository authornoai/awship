﻿using BLLogic.Nodes;
using UnityEngine;

namespace BLLogic.Action.Transforms
{
    public class LNPointParent : LogicNodeAction
    {
        [Input(connectionType = ConnectionType.Override), SerializeField]
        private Transform _pointParent;

        [Input(connectionType = ConnectionType.Override), SerializeField]
        private Transform _toParent;
        public override void Use_Start()
        {
        }

        public override void Use_Stop()
        {
            var visual = _pointParent.GetChild(0);
            visual.SetParent(_toParent);
            visual.gameObject.SetActive(false);
        }

        public override void Use_Interrupt()
        {
        }

        public override void FillValues()
        {
        }
    }
}