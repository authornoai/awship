﻿using BLLogic.Nodes;
using DG.Tweening;
using UnityEngine;

namespace BLLogic.Action.Transforms
{
    public class LNLerpToTarget : LogicNodeAction
    {
        [Input(connectionType = ConnectionType.Override), SerializeField]
        private Transform _toLerp;

        [Input(connectionType = ConnectionType.Override), SerializeField]
        private Transform _target;

        [SerializeField] private float _duration;

        public override void Use_Start()
        {
            _toLerp.DOMove(_target.position, _duration).SetEase(Ease.Linear);
        }

        public override void Use_Stop()
        {
        }

        public override void Use_Interrupt()
        {
        }

        public override void FillValues()
        {
            FillValue(ref _toLerp, nameof(_toLerp));
            FillValue(ref _target, nameof(_target));
        }
    }
}