﻿using BLLogic.Nodes;
using UnityEngine;

namespace BLLogic.Action.Transforms
{
    public class LNTransformTo : LogicNodeAction
    {
        [Input(connectionType = ConnectionType.Override), SerializeField]
        private Transform _target;

        [Input(connectionType = ConnectionType.Override), SerializeField]
        private Transform _to;

        [Input(connectionType = ConnectionType.Override), SerializeField]
        private Vector3 _pos;

        [SerializeField] private bool _setRotation;
        public override void Use_Start()
        {
            _target.position = _to.position + _pos;
            if (_setRotation) _target.rotation = _to.rotation;
        }

        public override void Use_Stop()
        {
        }

        public override void Use_Interrupt()
        {
        }

        public override void FillValues()
        {
            FillValue(ref _target, nameof(_target));
            FillValue(ref _to, nameof(_to));
            FillValue(ref _pos, nameof(_pos));
        }
    }
}