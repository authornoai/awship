﻿using System;
using BLLogic.Nodes;
using UnityEngine;

namespace BLLogic.Action.Debug
{
    public class LNDebugLog : LogicNodeAction
    {
        [SerializeField] private string _text;

        public override void Use_Start()
        {
            UnityEngine.Debug.Log(_text);
        }

        public override void Use_Stop()
        {
        }

        public override void Use_Interrupt()
        {
        }

        public override void FillValues()
        {
        }
    }
}