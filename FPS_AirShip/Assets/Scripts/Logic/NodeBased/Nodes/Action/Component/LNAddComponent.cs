﻿using BLLogic.Nodes;
using UnityEngine;

namespace BLLogic.Action.Component
{
    public abstract class LNAddComponent<T> : LogicNodeAction where T : UnityEngine.Component
    {
        [Input(connectionType = ConnectionType.Override), SerializeField] private GameObject _target;
        public override void FillValues()
        {
            FillValue(ref _target, nameof(_target));
        }

        public override void Use_Start() => _target.AddComponent<T>();
        public override void Use_Stop() { }
        public override void Use_Interrupt() { }
    }
}