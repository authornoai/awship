﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BLLogic.Context;
using BLLogic.Nodes;
using BLLogic.Special;
using UnityEngine;
using XNode;
using System;
using Runtime;

#if UNITY_EDITOR
using XNodeEditor;
using UnityEditor;
#endif

namespace BL.LogicBlock.NodeBased.Graph
{
    [CreateAssetMenu(fileName = "LSeq_", menuName = "SibKoala/Logic/Graph")]
    public class LogicGraph : NodeGraph
    {
        [SerializeField] private LogicNodeActionOne[] _actions;

        [SerializeField] private LNGetTranslated[] _translateTargets;
        [SerializeField] private LNGetBool[] _translateBoolNodes;
        [SerializeField] private LNGetInt[] _translateIntNodes;
        [SerializeField] private LNGetFloat[] _translateFloatNodes;
        [SerializeField] private LNGetVector3[] _translateVec3Nodes;
        
        public LogicNodeActionOne[] Actions => _actions;
        
        public LNGetTranslated[] TranslatedNodes => _translateTargets;
        public LNGetBool[] TranslatedBoolNodes => _translateBoolNodes;
        public LNGetFloat[] TranslatedFloatNodes => _translateFloatNodes;
        public LNGetInt[] TranslatedIntNodes => _translateIntNodes;
        public LNGetVector3[] TranslatedVec3NodesNodes => _translateVec3Nodes;

        private GameObject _user;
        private GameObject _useSource;
        private LNStart _startNode;
        
        public GameObject User => _user;
        public GameObject UseSource => _useSource;

        private Dictionary<string, GameObject> _translated;
        private Dictionary<string, int> _translatedInts;
        private Dictionary<string, float> _translatedFloats;
        private Dictionary<string, Vector3> _translatedVecs3;
        private Dictionary<string, bool> _translatedBools;
        public Dictionary<string, GameObject> Translated => _translated;
        public Dictionary<string, int> TranslatedInts => _translatedInts;
        public Dictionary<string, float> TranslatedFloats => _translatedFloats;
        public Dictionary<string, Vector3> TranslatedVecs3 => _translatedVecs3;
        public Dictionary<string, bool> TranslatedBools => _translatedBools;
        
        public Dictionary<Type, IDictionary> TranslatedAll { get; private set; }
        private Type[] _usedTranslatedTypes;

#if UNITY_EDITOR

        private FrameCooldown _cooldown;
        private double _cooldownStart;
        
        private void OnEnable()
        {
            if(nodes.Count > 0) return;
            _cooldown = new FrameCooldown();
            _cooldownStart = EditorApplication.timeSinceStartup;
            _cooldown.Capture(0);
            EditorApplication.update += CreateStartNode;
        }
        public void UpdateTranslatedTargets()
        {
            FillNodesToArrayByType(out _translateTargets);
            FillNodesToArrayByType(out _translateBoolNodes);
            FillNodesToArrayByType(out _translateIntNodes);
            FillNodesToArrayByType(out _translateFloatNodes);
            FillNodesToArrayByType(out _translateVec3Nodes);
        }
        private void FillNodesToArrayByType<T>(out T[] array)
        {
            array = nodes.Filter(x => x.GetType() == typeof(T))
                    .Cast<T>().ToArray();
        }

        private void CreateStartNode()
        {
            if(_cooldown.IsInCooldown(0.1f, (float)(EditorApplication.timeSinceStartup - _cooldownStart)))
            {
                return;
            }

            EditorApplication.update -= CreateStartNode;
            
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            Undo.RecordObject(this, "Create Start Node");
            Node startNode = AddNode(typeof(LNStart));
            
            if (startNode == null) return;

            Undo.RegisterCreatedObjectUndo(startNode, "Create Start Node");
            startNode.position = Vector2.zero;
            
            if (startNode.name.Trim() == "") startNode.name = NodeEditorUtilities.NodeDefaultName(startNode.GetType());
            if (!string.IsNullOrEmpty(AssetDatabase.GetAssetPath(this))) AssetDatabase.AddObjectToAsset(startNode, this);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            NodeEditorWindow.RepaintAll();
        }
#endif
        
        public void SetUser(GameObject user) => _user = user;
        public void SetSource(GameObject go) => _useSource = go;
        public void SetTranslated(
            GameObject[] go,
            int[] ints, float[] floats,
            Vector3[] vecs3, bool[] bools)
        {
            if (_usedTranslatedTypes != null)
            {
                for (int i = 0; i < _usedTranslatedTypes.Length; i++)
                {
                    TranslatedAll[_usedTranslatedTypes[i]].Clear();
                } 
            }

            FillTranslatedRealizations(go, Translated, _translateTargets);
            FillTranslatedRealizations(ints, TranslatedInts, _translateIntNodes);
            FillTranslatedRealizations(floats, TranslatedFloats, _translateFloatNodes);
            FillTranslatedRealizations(vecs3, TranslatedVecs3, _translateVec3Nodes);
            FillTranslatedRealizations(bools, TranslatedBools, _translateBoolNodes);
        }

        private void FillTranslatedRealizations<T>(T[] addFrom, Dictionary<string, T> addTo,
            IReadOnlyList<IGetTranslated> nodes)
        {
            if(addFrom == null || nodes == null) return;
            
#if UNITY_EDITOR
            if (addFrom?.Length != nodes.Count)
            {
                Debug.LogError($"{name}: Translated {nameof(T)} don't match with set lenght" +
                               $"\nCheck LogicSequence, they should have {nodes.Count} targets");
            }
#endif
            for (int i = 0; i < addFrom.Length; i++)
            {
                addTo.Add(nodes[i].GetKey(), addFrom[i]);
            }
        }

        public void InitData()
        {
            TranslatedAll = new Dictionary<Type, IDictionary>();

            InitTranslatedDictionary<Dictionary<string, GameObject>, GameObject>(out _translated);
            InitTranslatedDictionary<Dictionary<string, bool>, bool>(out _translatedBools);
            InitTranslatedDictionary<Dictionary<string, int>, int>(out _translatedInts);
            InitTranslatedDictionary<Dictionary<string, float>, float>(out _translatedFloats);
            InitTranslatedDictionary<Dictionary<string, Vector3>, Vector3>(out _translatedVecs3);

            _usedTranslatedTypes = TranslatedAll.Keys.ToArray();

            List<LogicNodeActionOne> actions = new List<LogicNodeActionOne>(1);
            List<LNParallelInside> addons = new List<LNParallelInside>();

            InitStoreNodesByType(actions, addons);
            InitActions(actions);
            InitSubGraphs(addons);

            _actions = actions.ToArray();
        }

        private void InitTranslatedDictionary<T, TU>(out T dictionary) where T : IDictionary, new()
        {
            dictionary = new T();
            TranslatedAll.Add(typeof(TU), dictionary);
        }
        private void InitStoreNodesByType(List<LogicNodeActionOne> actions, List<LNParallelInside> addons)
        {
            foreach (Node node in nodes)
            {
                switch (node)
                {
                    case LNStart startNode:
                        actions.Add(startNode);
                        _startNode = startNode;
                        break;
                    case LNParallelInside nodeAddon:
                        addons.Add(nodeAddon);
                        break;
                }
            }
        }
        private static void InitSubGraphs(List<LNParallelInside> addons)
        {
            for (int i = 0; i < addons.Count; i++)
            {
                addons[i].FillActions();
            }
        }
        private static void InitActions(List<LogicNodeActionOne> actions)
        {
            LogicNodeActionOne actionNode = actions[0];
            NodePort port = actionNode.GetOutputPort("_next");
            while (port != null)
            {
                Node nextNode = port.Connection?.node;
                if (nextNode is LogicNodeActionOne actionToPut)
                {
                    actions.Add(actionToPut);
                    port = actionToPut.GetOutputPort("_next");
                }
                else
                {
                    port = null;
                }
            }
        }
    }
}