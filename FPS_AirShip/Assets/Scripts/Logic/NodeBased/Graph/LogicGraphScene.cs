﻿using BL.KoalaLogic.LogicBlock;
using UnityEngine;
using XNode;

namespace BL.LogicBlock.NodeBased.Graph
{
    [RequireComponent(typeof(LogicSequence))]
    public class LogicGraphScene : SceneGraph<LogicGraph>
    {
    }
}