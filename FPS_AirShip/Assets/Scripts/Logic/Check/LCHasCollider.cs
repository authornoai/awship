﻿using Runtime;
using UnityEngine;

namespace BL.KoalaLogic.Check
{
    public class LCHasCollider : LogicCheckBase
    {
        [SerializeField] private Collider _target;
        [SerializeField] private bool _valueToPass;
        public override bool IsPassed(GameObject user) => _target.enabled == _valueToPass;
    }
}