﻿using Runtime;
using UnityEngine;

namespace BL.KoalaLogic.Check
{
    public class LCHasGOEnabled:LogicCheckBase
    {
        [SerializeField] private GameObject _lcGO;
        public override bool IsPassed(GameObject user) => _lcGO.activeInHierarchy;
    }
}