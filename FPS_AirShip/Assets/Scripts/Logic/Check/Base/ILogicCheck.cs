﻿using UnityEngine;

namespace BL.KoalaLogic.Check
{
    public interface ILogicCheck
    {
        bool IsPassed(GameObject user);
    }
}