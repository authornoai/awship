﻿using Runtime;
using UnityEngine;

namespace BL.KoalaLogic.Check
{
    public abstract class LogicCheckBase : MonoBehaviour, ILogicCheck
    {
        public abstract bool IsPassed(GameObject user);
    }

  
    
}