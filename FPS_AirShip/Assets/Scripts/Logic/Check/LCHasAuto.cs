﻿using Runtime;
using UnityEngine;

namespace BL.KoalaLogic.Check
{
    public class LCHasAuto : LogicCheckBase
    {
        public override bool IsPassed(GameObject user) => false;
    }
}